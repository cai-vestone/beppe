from .base import *  # noqa
from .base import env

# General settings
# -----------------------------------------------------------------------------

DEBUG = True


# Security settings
# -----------------------------------------------------------------------------

# Hosts/domain names that are valid for this site
# https://docs.djangoproject.com/en/dev/ref/settings/#allowed-hosts
ALLOWED_HOSTS = env.list('DJANGO_ALLOWED_HOSTS', default=['*'])

# See https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
SECRET_KEY = env(
    'DJANGO_SECRET_KEY',
    default='SET DJANGO_SECRET_KEY!',
)


# Cache settings
# -----------------------------------------------------------------------------

# A dictionary containing the settings for all caches to be used with Django.
# https://docs.djangoproject.com/en/dev/ref/settings/#caches
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': '',
    }
}


# Email settings
# -----------------------------------------------------------------------------

# https://docs.djangoproject.com/en/dev/ref/settings/#email-host
EMAIL_HOST = 'localhost'

# https://docs.djangoproject.com/en/dev/ref/settings/#email-port
EMAIL_PORT = 1025

# WhiteNoise
# ------------------------------------------------------------------------------
# http://whitenoise.evans.io/en/latest/django.html#using-whitenoise-in-development
INSTALLED_APPS = [
    'whitenoise.runserver_nostatic',  # let WhiteNoise serve static files
] + INSTALLED_APPS  # noqa F405


# Django debug toolbar settings
# See https://django-debug-toolbar.readthedocs.io/en/latest/configuration.html
# -----------------------------------------------------------------------------

# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#prerequisites
INSTALLED_APPS += ['debug_toolbar']  # noqa F405

# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#middleware
MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware']  # noqa F405

# https://django-debug-toolbar.readthedocs.io/en/latest/configuration.html#debug-toolbar-config  # noqa E501
DEBUG_TOOLBAR_CONFIG = {
    'DISABLE_PANELS': ['debug_toolbar.panels.redirects.RedirectsPanel'],
    'SHOW_TEMPLATE_CONTEXT': True,
}

# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#internal-ips
# INTERNAL_IPS = ['127.0.0.1', '10.0.2.2']


# django-extensions settings
# See https://django-extensions.readthedocs.io/en/latest/installation_instructions.html#configuration  # noqa E501
# -----------------------------------------------------------------------------

if 'django_extensions' not in INSTALLED_APPS:
    INSTALLED_APPS += ['django_extensions']  # noqa F405


# Your stuff...
# -----------------------------------------------------------------------------
