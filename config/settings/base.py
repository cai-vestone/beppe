"""
Common Django settings for beppe project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""

from pathlib import Path

import environ

from django.utils.translation import gettext_lazy as _

# Paths
# -----------------------------------------------------------------------------

ROOT_DIR = Path(__file__).resolve(strict=True).parent.parent.parent

# beppe/
APPS_DIR = ROOT_DIR / 'beppe'


# django-environ setup
# -----------------------------------------------------------------------------

env = environ.Env()
READ_DOT_ENV_FILE = env.bool('DJANGO_READ_DOT_ENV_FILE', default=False)
if READ_DOT_ENV_FILE:
    # OS environment variables take precedence over variables from .env
    env.read_env(str(ROOT_DIR / '.env'))


# General settings
# -----------------------------------------------------------------------------

# https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = env.bool('DJANGO_DEBUG', False)

# Python path to the root urlconf.
# https://docs.djangoproject.com/en/dev/ref/settings/#root-urlconf
ROOT_URLCONF = 'config.urls'

# The full Python path of the WSGI application object that Django’s built-in
# servers.
# https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'config.wsgi.application'

# https://docs.djangoproject.com/en/dev/ref/settings/#databases

DATABASES = {'default': env.db('DATABASE_URL', default="postgis:///beppe")}
DATABASES['default']['ATOMIC_REQUESTS'] = True

# Default primary key field type to use for models that don’t have a field
# with primary_key=True.
# see https://docs.djangoproject.com/en/dev/ref/settings/#default-auto-field
DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

# A dictionary specifying the package where migration modules can be found on a
# per-app basis.
# https://docs.djangoproject.com/en/dev/ref/settings/#migration-modules
MIGRATION_MODULES = {
    # overwrite the migrations for contrib.sites
    'sites': 'beppe.contrib.sites.migrations',
}

# List of directories searched for fixture files, in addition to the fixtures
# directory of each application, in search order.
# https://docs.djangoproject.com/en/dev/ref/settings/#fixture-dirs
FIXTURE_DIRS = (str(APPS_DIR / 'fixtures'),)

# The ID, as an integer, of the current site in the django_site database table.
# https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = env.int('DJANGO_SITE_ID', 1)

# A list of middleware to use.
# https://docs.djangoproject.com/en/dev/ref/settings/#middleware
MIDDLEWARE = [
    # see http://docs.django-cms.org/en/latest/how_to/apphooks.html#reloading-apphooks    # noqa E501
    'cms.middleware.utils.ApphookReloadMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    # 'django.middleware.common.BrokenLinkEmailsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'allauth.account.middleware.AccountMiddleware',
    # see http://docs.django-cms.org/en/latest/how_to/install.html#middleware
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',
]


# GDAL settings
# see https://docs.djangoproject.com/en/4.0/ref/contrib/gis/gdal/#settings
# -----------------------------------------------------------------------------

# A string specifying the location of the GDAL library. Typically, this setting
# is only used if the GDAL library is in a non-standard location.
# https://docs.djangoproject.com/en/dev/ref/contrib/gis/gdal/#gdal-library-path
GDAL_LIBRARY_PATH = env.str('GDAL_LIBRARY_PATH', None)


# GEOS settings
# see https://docs.djangoproject.com/en/4.0/ref/contrib/gis/geos/#settings
# -----------------------------------------------------------------------------

# A string specifying the location of the GEOS C library. Typically, this setting
# is only used if the GEOS C library is in a non-standard location.
# https://docs.djangoproject.com/en/dev/ref/contrib/gis/geos/#geos-library-path
GEOS_LIBRARY_PATH = env.str('GEOS_LIBRARY_PATH', None)


# Security settings
# -----------------------------------------------------------------------------

# https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
# SECRET_KEY = env('DJANGO_SECRET_KEY')

# Whether to use HttpOnly flag on the session cookie.
# https://docs.djangoproject.com/en/dev/ref/settings/#session-cookie-httponly
SESSION_COOKIE_HTTPONLY = True

# Whether to use HttpOnly flag on the CSRF cookie.
# https://docs.djangoproject.com/en/dev/ref/settings/#csrf-cookie-httponly
CSRF_COOKIE_HTTPONLY = True

# https://docs.djangoproject.com/en/dev/ref/settings/#secure-browser-xss-filter
SECURE_BROWSER_XSS_FILTER = True

# Default value for the X-Frame-Options header.
# https://docs.djangoproject.com/en/dev/ref/settings/#x-frame-options
X_FRAME_OPTIONS = 'SAMEORIGIN'  # django-cms needs at least 'SAMEORIGIN'

# Password hashers.
# https://docs.djangoproject.com/en/dev/ref/settings/#password-hashers
PASSWORD_HASHERS = [
    # https://docs.djangoproject.com/en/dev/topics/auth/passwords/#using-argon2-with-django  # noqa E501
    'django.contrib.auth.hashers.Argon2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
]

# Password validation.
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation' '.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation' '.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation' '.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation' '.NumericPasswordValidator',
    },
]


# Logging settings
# https://docs.djangoproject.com/en/dev/topics/logging
# -----------------------------------------------------------------------------

# https://docs.djangoproject.com/en/dev/ref/settings/#logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {'format': '%(levelname)s %(asctime)s %(module)s ' '%(process)d %(thread)d %(message)s'}
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        }
    },
    'root': {
        'level': 'INFO',
        'handlers': ['console'],
    },
}


# Authentication settings
# -----------------------------------------------------------------------------

# A list of authentication backend classes (as strings) to use when attempting
# to authenticate a user.
# https://docs.djangoproject.com/en/dev/ref/settings/#authentication-backends
AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
]

# The model to use to represent a User.
# https://docs.djangoproject.com/en/dev/ref/settings/#auth-user-model
AUTH_USER_MODEL = 'users.User'

# The URL or named URL pattern where requests are redirected after login when
# the LoginView doesn’t get a next GET parameter.
# https://docs.djangoproject.com/en/dev/ref/settings/#login-redirect-url
LOGIN_REDIRECT_URL = 'users:redirect'

# The URL or named URL pattern where requests are redirected for login when
# using the login_required() decorator, LoginRequiredMixin, or AccessMixin.
# https://docs.djangoproject.com/en/dev/ref/settings/#login-url
LOGIN_URL = 'account_login'


# Apps
# -----------------------------------------------------------------------------

DJANGO_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # 'django.contrib.humanize',  # Handy template tags
    'djangocms_admin_style',
    'beppe.admin.apps.BeppeAdminConfig',  # replaces django.contrib.admin
    'django.forms',
    'django.contrib.sitemaps',
    'beppe.apps.users.apps.UsersConfig',
]

THIRD_PARTY_APPS = [
    # django-allauth
    'allauth',
    'allauth.account',
    # django-cms
    'cms',
    'menus',
    'treebeard',
    'sekizai',
    # django-filer
    'filer',
    'easy_thumbnails',
    'adminsortable2',
    # django-leaflet
    'leaflet',
    # django-filters
    'django_filters',
    # djangocms-page-meta
    'meta',
    'djangocms_page_meta',
    # djangocms-blog
    'aldryn_apphooks_config',
    'parler',
    'taggit',
    'taggit_autosuggest',
    'sortedm2m',
    'djangocms_blog',
    # django-ftpserver
    'django_ftpserver',
]

CMS_PLUGINS = [
    'djangocms_text_ckeditor',
    'djangocms_link',
    'djangocms_file',
    'djangocms_picture',
    'djangocms_video',
    'beppe.cms_plugins.picture',  # djangocms_picture
    'beppe.cms_plugins.link',  # djangocms_link
    'beppe.cms_plugins.picture_gallery',
    'beppe.cms_plugins.callout',
    'beppe.cms_plugins.webcam',
]

LOCAL_APPS = [
    # 'beppe.apps.users',  # It's in DJANGO_APPS
    'beppe.apps.lodovico',
    'beppe.apps.enrico',
    'beppe.apps.alcide',
    'beppe.apps.cesira',
    'beppe.apps.marta',  # component library
]

# A list of strings designating all applications that are enabled in this
# Django installation.
# https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + CMS_PLUGINS + LOCAL_APPS


# Internationalization settings
# see https://docs.djangoproject.com/en/2.2/topics/i18n/
# -----------------------------------------------------------------------------

# The language code for this installation.
# https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = env('DJANGO_LANGUAGE_CODE', default='en-us')

# The time zone for this installation.
# https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-TIME_ZONE
TIME_ZONE = env('DJANGO_TIME_ZONE', default='UTC')

# Whether Django’s translation system should be enabled.
# https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-USE_I18N
USE_I18N = True

# Specify if localized formatting of data will be enabled by default or not.
# https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True

# Specifies if datetimes will be timezone-aware by default or not.
# https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True

# This specifies which languages are available for language selection.
# https://docs.djangoproject.com/en/dev/ref/settings/#languages
LANGUAGES = (
    ('it', _('Italian')),
    ('en', _('English')),
)

# A list of directories where Django looks for translation files.
# https://docs.djangoproject.com/en/dev/ref/settings/#locale-paths
LOCALE_PATHS = [
    str(APPS_DIR / 'locale'),
    str(ROOT_DIR / 'config' / 'locale'),
]


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/dev/howto/static-files/
# https://docs.djangoproject.com/en/dev/topics/files/
# -----------------------------------------------------------------------------

# URL that handles the media served from MEDIA_ROOT.
# https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = env('DJANGO_MEDIA_URL', default='/media/')

# Absolute filesystem path to the directory that will hold user-uploaded files.
# https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = str(APPS_DIR / 'media')

# URL to use when referring to static files located in STATIC_ROOT.
# https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = env('DJANGO_STATIC_URL', default='/static/')

# Absolute path to the directory where collectstatic will collect static files
# for deployment.
# https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = str(ROOT_DIR / 'staticfiles')

# Additional locations the staticfiles app will traverse if the
# FileSystemFinder finder is enabled.
# https://docs.djangoproject.com/en/dev/ref/settings/#staticfiles-dirs
STATICFILES_DIRS = [
    str(APPS_DIR / 'static'),
]

# The list of finder backends that know how to find static files in various
# locations.
# https://docs.djangoproject.com/en/dev/ref/settings/#staticfiles-finders
STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]


# Template settings
# -----------------------------------------------------------------------------

# A list containing the settings for all template engines to be used.
# https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES = [
    {
        # https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-TEMPLATES-BACKEND  # noqa E501
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        # https://docs.djangoproject.com/en/dev/ref/settings/#template-dirs
        'DIRS': [str(APPS_DIR / 'templates')],
        # https://docs.djangoproject.com/en/dev/ref/settings/#app-dirs
        'APP_DIRS': True,
        'OPTIONS': {
            # https://docs.djangoproject.com/en/dev/ref/settings/#template-context-processors  # noqa E501
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'beppe.utils.context_processors.settings_context',
                # see http://docs.django-cms.org/en/latest/how_to/install.html#sekizai  # noqa E501
                'sekizai.context_processors.sekizai',
                # see http://docs.django-cms.org/en/latest/how_to/install.html#context-processors  # noqa E501
                'cms.context_processors.cms_settings',
            ],
        },
    }
]

# The class that renders form widgets.
# https://docs.djangoproject.com/en/dev/ref/settings/#form-renderer
FORM_RENDERER = 'django.forms.renderers.TemplatesSetting'


# Email settings
# -----------------------------------------------------------------------------

# The backend to use for sending emails.
# https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND', default='django.core.mail.backends.smtp.EmailBackend')

# Timeout in seconds for blocking operations like the connection attempt.
# https://docs.djangoproject.com/en/dev/ref/settings/#email-timeout
EMAIL_TIMEOUT = 5


# Admin settings
# -----------------------------------------------------------------------------

# Show the navigation sidebar in admin pages
# https://docs.djangoproject.com/en/dev/ref/contrib/admin/#django.contrib.admin.AdminSite.enable_nav_sidebar  # noqa E501
ADMIN_SHOW_SIDEBAR = env.bool('ADMIN_SITE_SHOW_SIDEBAR', default=False)

# The text to put at the top of each admin page
# see https://docs.djangoproject.com/en/dev/ref/contrib/admin/#django.contrib.admin.AdminSite.site_header  # noqa E501
ADMIN_SITE_HEADER = env('ADMIN_SITE_HEADER', default='Beppe')

# The text to put at the end of each admin page’s <title>
# see https://docs.djangoproject.com/en/dev/ref/contrib/admin/#django.contrib.admin.AdminSite.site_title  # noqa E501
ADMIN_SITE_TITLE = env('ADMIN_SITE_TITLE', default=ADMIN_SITE_HEADER)

# The text to put at the top of the admin index page.
# see https://docs.djangoproject.com/en/dev/ref/contrib/admin/#django.contrib.admin.AdminSite.index_title  # noqa E501
ADMIN_INDEX_TITLE = env.str('ADMIN_SITE_INDEX_TITLE', default='Beppe administration')

# Django Admin URL.
ADMIN_URL = 'admin/'

# https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = [
    ('geobaldi', 'geobaldi@icloud.com'),
]

# https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS


# Django REST framework settings
# See https://www.django-rest-framework.org/api-guide/settings/
# -----------------------------------------------------------------------------

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': ('rest_framework.permissions.IsAuthenticated',),
}

# django-cors-headers
# https://github.com/adamchainz/django-cors-headers#setup
CORS_URLS_REGEX = r'^/api/.*$'


# django-allauth settings
# See https://django-allauth.readthedocs.io/en/latest/configuration.html
# -----------------------------------------------------------------------------

# Allow user signup
ACCOUNT_ALLOW_REGISTRATION = env.bool('DJANGO_ACCOUNT_ALLOW_REGISTRATION', default=True)

# Specifies the login method to use
# 'username' | 'email' | 'username_email'
ACCOUNT_AUTHENTICATION_METHOD = 'username'

# The user is required to hand over an e-mail address when signing up.
ACCOUNT_EMAIL_REQUIRED = True

# Determines the e-mail verification method during signup.
# When set to “mandatory” the user is blocked from logging in until the
# email address is verified.
# 'mandatory' | 'optional' | 'none'.
ACCOUNT_EMAIL_VERIFICATION = 'mandatory'

ACCOUNT_USER_DISPLAY = 'beppe.utils.account.user_display'

# Specifies the adapter class to use, allowing you to alter certain default
# behaviour.
ACCOUNT_ADAPTER = 'beppe.apps.users.adapters.AccountAdapter'

ACCOUNT_FORMS = {
    # 'login': 'allauth.account.forms.LoginForm',
    'signup': 'beppe.apps.users.forms.SignupForm',
    # 'add_email': 'allauth.account.forms.AddEmailForm',
    # 'change_password': 'allauth.account.forms.ChangePasswordForm',
    # 'set_password': 'allauth.account.forms.SetPasswordForm',
    # 'reset_password': 'allauth.account.forms.ResetPasswordForm',
    # 'reset_password_from_key': 'allauth.account.forms.ResetPasswordKeyForm',
}


# CMS configuration
# see http://docs.django-cms.org/en/latest/reference/configuration.html
# -----------------------------------------------------------------------------

# Languages
# see http://docs.django-cms.org/en/latest/reference/configuration.html#cms-languages  # noqa E501
CMS_LANGUAGES = {
    1: [
        {
            'code': 'it',
            'name': _('Italian'),
            'redirect_on_fallback': True,
            'public': True,
            'hide_untranslated': False,
        },
    ],
    'default': {
        'redirect_on_fallback': True,
        'public': True,
        'hide_untranslated': False,
    },
}

CMS_TEMPLATES = (
    ('beppe/pages/page.html', _('Page')),
    ('beppe/pages/untitled_page.html', _('Untitled')),
)

# Use permissions on pages
# see http://docs.django-cms.org/en/latest/reference/configuration.html#cms-permission  # noqa E501
CMS_PERMISSION = True

# Placeholder configuration
# see http://docs.django-cms.org/en/latest/reference/configuration.html#cms-placeholder-conf  # noqa E501
CMS_PLACEHOLDER_CONF = {
    None: {
        'extra_context': {
            'width': 1200,
        },
    },
    # CMS page content
    'content': {
        'name': _('Content'),
    },
    # Blog post content
    'post_content': {
        'name': _('Content'),
    },
    'header': {
        'name': _('Header'),
        'plugins': ['PicturePlugin'],
        'extra_context': {
            'width': 2500,
        },
        'limits': {
            'global': 1,
        },
    },
    'trail_header': {
        'name': _('Header'),
        'plugins': ['PicturePlugin'],
        'limits': {
            'global': 1,
        },
    },
    'leg_header': {
        'name': _('Header'),
        'plugins': ['PicturePlugin'],
        'limits': {
            'global': 1,
        },
    },
    'trip_header': {
        'name': _('Header'),
        'plugins': ['PicturePlugin'],
        'limits': {
            'global': 1,
        },
    },
    'footer': {
        'name': _('Footer'),
        'plugins': ['TextPlugin'],
    },
    'trail_description': {
        'name': _('Description'),
        'plugins': ['TextPlugin'],
    },
    'leg_description': {
        'name': _('Description'),
        'plugins': ['TextPlugin'],
    },
    'trip_report': {
        'name': _('Report'),
        'plugins': ['TextPlugin'],
    },
    'trip_pictures': {
        'name': _('Pictures'),
        'plugins': ['PolaroidsPlugin'],
    },
}

# Plugin processors
# see http://docs.django-cms.org/en/latest/how_to/custom_plugins.html#plugin-processors  # noqa E501
# CMS_PLUGIN_PROCESSORS = ()


# easy-thumbnails settings
# see http://easy-thumbnails.readthedocs.io/en/stable/ref/settings/
# -----------------------------------------------------------------------------

THUMBNAIL_HIGH_RESOLUTION = False
THUMBNAIL_QUALITY = 85

THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
)

THUMBNAIL_DEBUG = DEBUG

THUMBNAIL_ALIASES = {
    '': {
        'default': {
            'size': (2000, 1333),
            'crop': True,
        },
        'trail-card': {
            'size': (500, 300),
            'crop': True,
        },
        'carousel': {
            'size': (1200, 804),
            'crop': True,
        },
        'carousel-aligned': {
            'size': (600, 402),
            'crop': True,
        },
        'polaroid-thumbnail': {
            'size': (300, 333),
            'crop': True,
        },
        'trip-flyer': {
            'size': (595, 842),
            'crop': False,
        },
        'magazineissue-cover': {
            'size': (400, 554),
            'crop': False,
        },
        # meta image
        'meta_image': {
            'size': (
                env.int('META_IMAGE_WIDTH', default=1200),
                env.int('META_IMAGE_HEIGHT', default=630),
            ),
            'crop': True,
        },
    },
}


# django-filer settings
# see https://django-filer.readthedocs.io/en/latest/settings.html
# -----------------------------------------------------------------------------

# File models
# see https://django-filer.readthedocs.io/en/latest/extending_filer.html#the-very-basics  # noqa E501
FILER_FILE_MODELS = (
    'lodovico.GpxFile',  # GPX files
    'filer.Image',  # Images
    'filer.File',  # Generic files'
)

# Activate the or not the Permission check on the files and folders before
# displaying them in the admin. When set to False it gives all the authorization
# to staff members based on standard Django model permissions.
# https://django-filer.readthedocs.io/en/latest/settings.html#filer-enable-permissions
FILER_ENABLE_PERMISSIONS = True

# Should newly uploaded files have permission checking disabled (be public) by default.
# https://django-filer.readthedocs.io/en/latest/settings.html#filer-is-public-default
FILER_IS_PUBLIC_DEFAULT = True


# djangocms-text-ckeditor settings
# see https://github.com/django-cms/djangocms-text-ckeditor#configuration
# -----------------------------------------------------------------------------

CKEDITOR_SETTINGS = {
    'language': '{{ language }}',
    'toolbar_CMS': [
        ['Undo', 'Redo'],
        ['cmsplugins', 'cm swidget', '-', 'ShowBlocks'],
        ['-', 'PasteText', 'PasteFromWord'],
        ['Format'],
        '/',
        ['Bold', 'Italic', 'Underline', 'Strike'],
        ['Subscript', 'Superscript', '-', 'RemoveFormat'],
        ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
        ['HorizontalRule'],
        ['NumberedList', 'BulletedList'],
        ['Outdent', 'Indent', '-', 'Blockquote', '-', '-'],
        ['Source'],
    ],
    'toolbar': 'CMS',
    'skin': 'moono-lisa',
    'contentsCss': STATIC_URL + 'marta/css/editor.css',
}


# djangocms-picture settings
# see https://github.com/django-cms/djangocms-picture#configuration
# -----------------------------------------------------------------------------

DJANGOCMS_PICTURE_RESPONSIVE_IMAGES = True
# DJANGOCMS_PICTURE_RESPONSIVE_IMAGES_VIEWPORT_BREAKPOINTS = [576, 768, 992]
# DJANGOCMS_PICTURE_RATIO = 1.618
# DJANGOCMS_PICTURE_NESTING = False


# djangocms-video settings
# see https://github.com/django-cms/djangocms-video#configuration
# -----------------------------------------------------------------------------

# DJANGOCMS_VIDEO_TEMPLATES = None
DJANGOCMS_VIDEO_ALLOWED_EXTENSIONS = ['mp4', 'webm', 'ogv']
DJANGOCMS_VIDEO_YOUTUBE_EMBED_URL = '//www.youtube-nocookie.com/embed/{}'


# django-leaflet settings
# see https://django-leaflet.readthedocs.io/en/latest/templates.html#configuration  # noqa E501
# -----------------------------------------------------------------------------

LEAFLET_CONFIG = {
    # You can configure a global spatial extent for your maps, that will
    # automatically center your maps, restrict panning and add reset view and
    # scale controls. (See advanced usage to tweak that)
    # 'SPATIAL_EXTENT': (5.0, 44.0, 7.5, 46),
    # In addition to limiting your maps with SPATIAL_EXTENT, you can also
    # specify initial map center, default, min and max zoom level
    # The tuple/list must contain (lat,lng) coords.
    'DEFAULT_CENTER': (45.662587, 10.429459),
    'DEFAULT_ZOOM': 12,
    'MIN_ZOOM': 3,
    'MAX_ZOOM': 15,
    # To globally add a tiles layer to your maps
    # This setting can also be a list of tuples (name, url, options). The
    # python dict options accepts all the Leaflet tileLayers options.
    # If it contains several layers, a layer switcher will then be added
    # automatically.
    # 'TILES': 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    'TILES': [
        (
            'Opentopomap',
            'https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png',
            {
                'attribution': 'Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)',  # noqa E501
            },
        ),
    ],
    # To globally add an overlay layer, use the same syntax as tiles.
    # Currently, overlay layers from settings are limited to tiles. For
    # vectorial overlays, you will have to add them via JavaScript
    # (see events).
    # 'OVERLAYS': [(
    #     'Cadastral',
    #     'http://server/a/{z}/{x}/{y}.png',
    #     {'attribution': '&copy; IGN'}
    # )],
    # 'OVERLAYS': [(
    #     'Lines',
    #     'http://{s}.tile.stamen.com/terrain-lines/{z}/{x}/{y}.png', {
    #         'maxZoom': 12,
    #     },
    # ),(
    #     'Labels',
    #     'http://{s}.tile.stamen.com/terrain-labels/{z}/{x}/{y}.png', {
    #         'maxZoom': 12,
    #     },
    # )],
    # To globally add an attribution prefix on maps (most likely an empty
    # string)
    # Default is None, which leaves the value to Leaflet's default.
    # 'ATTRIBUTION_PREFIX': 'Powered by django-leaflet'
    # Scale control may be set to show 'metric' (m/km), or 'imperial' (mi/ft)
    # scale lines, or 'both'.
    'SCALE': 'metric',
    # Shows a small map in the corner which shows the same as the main map
    # with a set zoom offset:
    'MINIMAP': False,
    # By default, a button appears below the zoom controls and, when clicked,
    # shows the entire map. To remove this button, set:
    'RESET_VIEW': False,
    # Since 0.7.0, the leaflet_map template tag no longer registers
    # initialization functions in global scope, and no longer adds map objects
    # into window.maps array by default. To restore these features, use:
    # 'NO_GLOBALS': False,
    # If you are using staticfiles compression libraries such as
    # django_compressor, which can do any of compressing, concatenating or
    # renaming javascript files, this may break Leaflet's own ability to
    # determine its installed path, and in turn break the method
    # L.Icon.Default.imagePath().
    # To use Django's own knowledge of its static files to force this value
    # explicitly, use:
    'FORCE_IMAGE_PATH': True,
    # To ease the usage of plugins, django-leaflet allows specifying a set of
    # plugins, that can later be referred to from the template tags by name
    # 'PLUGINS': {
    #     'name-of-plugin': {
    #         'css': [
    #             'relative/path/to/stylesheet.css',
    #             '/root/path/to/stylesheet.css'],
    #         'js': 'http://absolute-url.example.com/path/to/script.js',
    #         'auto-include': True,
    #     },
    # }
    'PLUGINS': {
        'fullscreen': {
            'css': [
                'https://cdn.jsdelivr.net/npm/leaflet-fullscreen@1.0.2/dist/leaflet.fullscreen.css',  # noqa E501
            ],
            'js': [
                'https://cdn.jsdelivr.net/npm/leaflet-fullscreen@1.0.2/dist/Leaflet.fullscreen.min.js',  # noqa E501
            ],
            'auto-include': True,
        },
        'gesture-handling': {
            'css': [
                'https://cdn.jsdelivr.net/npm/leaflet-gesture-handling@1.2.2/dist/leaflet-gesture-handling.min.css',  # noqa E501
            ],
            'js': [
                'https://cdn.jsdelivr.net/npm/leaflet-gesture-handling@1.2.2/dist/leaflet-gesture-handling.min.js',  # noqa E501
            ],
            'auto-include': True,
        },
        'markercluster': {
            'css': [
                'https://cdn.jsdelivr.net/npm/leaflet.markercluster@1.5.3/dist/MarkerCluster.css',  # noqa E501
                'https://cdn.jsdelivr.net/npm/leaflet.markercluster@1.5.3/dist/MarkerCluster.Default.css',  # noqa E501
            ],
            'js': [
                'https://cdn.jsdelivr.net/npm/leaflet.markercluster@1.5.3/dist/leaflet.markercluster.js',  # noqa E501
            ],
            'auto-include': True,
        },
        'print': {
            'css': [],
            'js': [
                'https://cdn.jsdelivr.net/npm/leaflet.browser.print@1.0.6/dist/leaflet.browser.print.min.js',  # noqa E501
            ],
            'auto-include': True,
        },
        'locate-control': {
            'css': [
                'https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css',  # noqa E501
                'https://cdn.jsdelivr.net/npm/leaflet.locatecontrol@0.76.0/dist/L.Control.Locate.min.css',  # noqa E501
            ],
            'js': [
                'https://cdn.jsdelivr.net/npm/leaflet.locatecontrol@0.76.0/dist/L.Control.Locate.min.js',  # noqa E501
            ],
            'auto-include': True,
        },
        'elevation': {
            'css': [
                'https://cdn.jsdelivr.net/npm/@raruto/leaflet-elevation@1.7.3/dist/leaflet-elevation.min.css'  # noqa E501
            ],
            'js': [
                'https://cdn.jsdelivr.net/npm/@raruto/leaflet-elevation@1.7.3/dist/leaflet-elevation.min.js',  # noqa E501
            ],
            'auto-include': True,
        },
    },
}


# django-filters settings
# -----------------------------------------------------------------------------

# Set the default value for ChoiceFilter.empty_label. You may disable the empty
# choice by setting this to None.
# see https://django-filter.readthedocs.io/en/master/ref/settings.html#filters-empty-choice-label  # noqa E501
# FILTERS_EMPTY_CHOICE_LABEL = '---------'

# Set the default value for ChoiceFilter.null_label. You may enable the null
# choice by setting a non-None value.
# see https://django-filter.readthedocs.io/en/master/ref/settings.html#filters-null-choice-label  # noqa E501
# FILTERS_NULL_CHOICE_LABEL = None

# This setting controls the verbose output for generated filter labels. Instead
# of getting expression parts such as “lt” and “contained_by”, the
# verbose label would contain “is less than” and “is contained by”.
# Verbose output may be disabled by setting this to a falsy value.
# see https://django-filter.readthedocs.io/en/master/ref/settings.html#filters-verbose-lookups  # noqa E501
# refer to 'django_filters.conf.DEFAULTS'
# FILTERS_VERBOSE_LOOKUPS = {
#     'exact': _(''),
#     'iexact': _(''),
#     'contains': _('contains'),
#     'icontains': _('contains'),
#     ...
# }


# django-meta settings
# https://django-meta.readthedocs.io/en/latest/settings.html
# -----------------------------------------------------------------------------

# Defines the protocol used on your site. This should be set to either 'http' or
# 'https'. Default is None.
META_SITE_PROTOCOL = env.str('META_SITE_PROTOCOL', default='http')

# Domain of your site. The Meta objects can also be made to use the Django’s
# Sites framework as well (see Meta object and META_USE_SITES sections). Default
# is None.
META_SITE_DOMAIN = None

# The default og:type property to use site-wide. You do not need to set this if
# you do not intend to use the OpenGraph properties. Default is None.
META_SITE_TYPE = env.str('META_SITE_TYPE', default='website')

# The site name to use in og:site_name property. Althoug this can be set per
# view, we recommend you set it globally. Defalt is None.
META_SITE_NAME = env.str('META_SITE_NAME', default='Beppe')

# Iterable of extra keywords to include in every view. These keywords are
# appended to whatever keywords you specify for the view, but are not used at
# all if no keywords are specified for the view. See META_DEFAULT_KEYWORDS if
# you wish to specify keywords to be used when no keywords are supplied. Default
# is [].
# META_INCLUDE_KEYWORDS = []

# Iterable of default keywords to use when no keywords are specified for the
# view. These keywords are not included if you specify keywords for the view. If
# you need keywords that will always be present, regardless of whether you’ve
# specified any other keywords for the view or not, you need to combine this
# setting with META_INCLUDE_KEYWORDS setting. Default is [].
# META_DEFAULT_KEYWORDS = []

# This setting is used as the base URL for all image assets that you intend to
# use as og:image property in your views. This is django-meta’s counterpart of
# the Django’s STATIC_URL setting. In fact, Django’s STATIC_URL setting is a
# fallback if you do not specify this setting, so make sure either one is
# configured. Default is to use the STATIC_URL setting.
# Note that you must add the trailing slash when specifying the URL. Even if you
# do not intend to use the og:image property, you need to define either this
# setting or the STATIC_URL setting or an attribute error will be raised.
# META_IMAGE_URL = STATIC_URL

# This setting tells django-meta whether to render the OpenGraph properties.
# Default is False.
META_USE_OG_PROPERTIES = True

# This setting tells django-meta whether to render the Twitter properties.
# Default is False.
META_USE_TWITTER_PROPERTIES = True

# This setting tells django-meta whether to render the Schema.org properties.
# Default is False.
META_USE_SCHEMAORG_PROPERTIES = True

# This setting tells django-meta whether to render the <title></title> tag.
# Default is False.
META_USE_TITLE_TAG = False

# This setting tells django-meta to derive the site’s domain using the
# Django’s sites contrib app. If you enable this setting, the META_SITE_DOMAIN
# is not used at all. Default is False.
META_USE_SITES = True

# Use this setting to add a list of additional OpenGraph namespaces to be
# declared in the <head> tag.
# META_OG_NAMESPACES = None

# Use this setting to customize the list of additional OpenGraph properties for
# which the :secure_url suffix is added if the URL value is a https URL.
# META_OG_SECURE_URL_ITEMS = ('image', 'audio', 'video')

# The following settings are available to set a default value to the
# corresponding attribute for both View support and Model support

# image: must be an absolute URL, ignores META_IMAGE_URL
META_DEFAULT_IMAGE = env.str('META_DEFAULT_IMAGE', default='images/adamello.jpg')

# og_type: default: first META_FB_TYPES
META_FB_TYPE = META_SITE_TYPE

# og_app_id
# META_FB_APPID = ''

# og_profile_id
# META_FB_PROFILE_ID = ''

# fb_pages
# META_FB_PAGES = ''

# og_publisher
# META_FB_PUBLISHER = ''

# og_author_url
# META_FB_AUTHOR_URL = ''

# twitter_type: default: first META_TWITTER_TYPES
# META_TWITTER_TYPE = ''

# twitter_site:  (default: blank)
# META_TWITTER_SITE = ''

# twitter_author:  (default: blank)
# META_TWITTER_AUTHOR = ''

# schemaorg_type:  (default: first META_SCHEMAORG_TYPE)
# META_SCHEMAORG_TYPE = ''


# djangocms-page-meta settings
# https://djangocms-page-meta.readthedocs.io/en/latest/configuration.html
# -----------------------------------------------------------------------------

# Set the max length of the HTML meta description field. Default is 320.
PAGE_META_DESCRIPTION_LENGTH = env.int('META_DESCRIPTION_LENGTH', default=320)


# Set the max length of the Twitter card description field. Default is 280.
# PAGE_META_TWITTER_DESCRIPTION_LENGTH = 280


# djangocms-blog settings
# https://djangocms-blog.readthedocs.io/en/latest/autodoc/settings.html
# -----------------------------------------------------------------------------

# This is a copy of CKEDITOR_SETTINGS without cms plugins
BLOG_CKEDITOR_SETTINGS = {
    'language': '{{ language }}',
    'toolbar_BLOG': [
        ['Undo', 'Redo'],
        ['cm swidget', '-', 'ShowBlocks'],
        ['-', 'PasteText', 'PasteFromWord'],
        ['Format'],
        '/',
        ['Bold', 'Italic', 'Underline', 'Strike'],
        ['Subscript', 'Superscript', '-', 'RemoveFormat'],
        ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
        ['HorizontalRule'],
        ['NumberedList', 'BulletedList'],
        ['Outdent', 'Indent', '-', 'Blockquote', '-', '-'],
        ['Source'],
    ],
    'toolbar': 'BLOG',
    'skin': 'moono-lisa',
    'contentsCss': STATIC_URL + 'marta/css/editor.css',
}

# Use an abstract field for the post.
# If False no abstract field is available for posts.
BLOG_USE_ABSTRACT = True

# Configuration for the CKEditor of the abstract field.
BLOG_ABSTRACT_CKEDITOR = BLOG_CKEDITOR_SETTINGS

# Post content is managed via placeholder.
# If False a HTMLField is provided instead.
# BLOG_USE_PLACEHOLDER = True

# Configuration for the CKEditor of the post content field.
BLOG_POST_TEXT_CKEDITOR = BLOG_CKEDITOR_SETTINGS

# Enable related posts to link one post to others.
# BLOG_USE_RELATED = True


# def fieldset_filter_function(fsets, request, obj=None):
#     fsets[0][1]['fields'] = [
#         'publish',
#         'title',
#         'subtitle',
#         'slug',
#         'main_image',
#         'categories',
#     ]
#     fsets[1][1]['fields'] = [
#         'author',
#         'related',
#     ]
#     fsets[2] = (
#         _('Dates'),
#         {
#             'fields': [
#                 'date_published',
#                 'date_published_end',
#                 'date_featured',
#             ],
#             'classes': ('collapse',),
#         },
#     )
#     del fsets[3]  # Remove Images
#     fsets.append(
#         (
#             _('Advanced settings'),
#             {
#                 'fields': [
#                     'app_config',
#                 ],
#                 'classes': ('collapse',),
#             },
#         )
#     )
#     return fsets


# Callable function to change (add or filter) fields to fieldsets for admin
# post edit form.
# BLOG_ADMIN_POST_FIELDSET_FILTER = fieldset_filter_function

# Whether to enable comments by default on posts.
# While djangocms_blog does not ship any comment system, this flag can be used
# to control the chosen comments framework.
# NOTE: if set to False this triggers a db migration
BLOG_ENABLE_COMMENTS = True

# Enable aldryn-search (i.e.: django-haystack) indexes.
BLOG_ENABLE_SEARCH = False

# Name of the djangocms-blog plugins module (group).
# BLOG_PLUGIN_MODULE_NAME = 'Blog'
# Name of the plugin showing the blog archive index.
BLOG_ARCHIVE_PLUGIN_NAME = _('Archive')
# Name of the plugin showing the list of posts per authors.
BLOG_AUTHOR_POSTS_LIST_PLUGIN_NAME = _('Author blog articles list')
# Name of the plugin showing the list of blog posts authors
BLOG_AUTHOR_POSTS_PLUGIN_NAME = _('Author blog articles')
# Name of the plugin showing the list of blog categories.
BLOG_CATEGORY_PLUGIN_NAME = _('Categories')
# Name of the plugin showing the list of latest posts.
BLOG_LATEST_ENTRIES_PLUGIN_NAME = _('Latest blog articles')
# Name of the plugin showing the list of latest posts (cached version).
BLOG_LATEST_ENTRIES_PLUGIN_NAME_CACHED = _('Latest blog articles - Cache')
# Name of the plugin showing the tag blog cloud.
BLOG_TAGS_PLUGIN_NAME = _('Tags')

# Default label for Blog item (used in django CMS Wizard).
BLOG_DEFAULT_OBJECT_NAME = _('Article')

# Use a default if not specified:
# - True: the current user is set as the default author;
# - False: no default author is set;
# - any other string: the user with the provided username is used;
BLOG_AUTHOR_DEFAULT = True

# Enable the blog Auto setup feature.
# BLOG_AUTO_SETUP = True
# Title of the BlogConfig instance created by Auto setup.
# BLOG_AUTO_APP_TITLE = 'Blog'
# Title of the blog page created by Auto setup.
# BLOG_AUTO_BLOG_TITLE = 'Blog'
# Title of the home page created by Auto setup.
BLOG_AUTO_HOME_TITLE = _('Home page')
# Namespace of the BlogConfig instance created by Auto setup.
# BLOG_AUTO_NAMESPACE = 'Blog'

# Choices of permalinks styles.
BLOG_AVAILABLE_PERMALINK_STYLES = (
    ('full_date', _('Full date')),
    ('short_date', _('Year / Month')),
    ('category', _('Category')),
    ('slug', _('Just slug')),
)

# URLConf corresponding to BLOG_AVAILABLE_PERMALINK_STYLES
# BLOG_PERMALINK_URLS = {
#     'category': '<str:category>/<str:slug>/',
#     'full_date': '<int:year>/<int:month>/<int:day>/<str:slug>/',
#     'short_date': '<int:year>/<int:month>/<str:slug>/',
#     'slug': '<str:slug>/'
# }

# Easy-thumbnail alias configuration for the post main image when shown on the
# post detail; it’s a dictionary with size, crop and upscale keys.
BLOG_IMAGE_FULL_SIZE = {'crop': True, 'size': '2000x1333', 'upscale': False}

# Easy-thumbnail alias configuration for the post main image when shown on the
# post lists; it’s a dictionary with size, crop and upscale keys.
BLOG_IMAGE_THUMBNAIL_SIZE = {'crop': True, 'size': '600x400', 'upscale': False}

# Default number of post in the Latest post plugin.
# NOTE: if set this triggers a db migration
BLOG_LATEST_POSTS = 5

# Flag to show / hide categories without posts attached from the menu.
BLOG_MENU_EMPTY_CATEGORIES = False

# List of available menu types.
BLOG_MENU_TYPES = (
    ('complete', _('Categories and posts')),
    ('categories', _('Categories only')),
    ('posts', _('Posts only')),
    ('none', _('None')),
)

# Add support for multisite setup.
BLOG_MULTISITE = False

# Number of post per page.
BLOG_PAGINATION = 10

# Default number of words shown for abstract in the post list.
BLOG_POSTS_LIST_TRUNCWORDS_COUNT = env.int('BLOG_POSTS_LIST_TRUNCWORDS_COUNT', default=20)


# Beppe settings
# -----------------------------------------------------------------------------

BEPPE_HEADER_BACKGROUNDS = [
    'marta/img/12-apostoli-01.jpg',
    'marta/img/12-apostoli-02.jpg',
    'marta/img/3-cime-01.jpg',
    'marta/img/3-cime-02.jpg',
    'marta/img/cresta-croce-01.jpg',
    'marta/img/don-zio-01.jpg',
    'marta/img/don-zio-02.jpg',
    'marta/img/don-zio-03.jpg',
    'marta/img/pasubio-03.jpg',
    'marta/img/sentiero-roma-01.jpg',
]


BEPPE_LOGO_CAPTION = env.str('BEPPE_LOGO_CAPTION', multiline=True, default=None)


# Lodovico settings
# -----------------------------------------------------------------------------

LODOVICO_TRAIL_ACTIVITY_ID = env.int('LODOVICO_TRAIL_ACTIVITY_ID', 1)
