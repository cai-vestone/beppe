from .base import *  # noqa
from .base import env

# General settings
# -----------------------------------------------------------------------------

DATABASES['default'] = env.db('DATABASE_URL')  # noqa F405
DATABASES['default']['ATOMIC_REQUESTS'] = False  # noqa F405
DATABASES['default']['CONN_MAX_AGE'] = env.int('DATABASE_CONN_MAX_AGE', default=60)  # noqa F405


# Security settings
# -----------------------------------------------------------------------------

# Hosts/domain names that are valid for this site.
# https://docs.djangoproject.com/en/dev/ref/settings/#allowed-hosts
ALLOWED_HOSTS = env.list('DJANGO_ALLOWED_HOSTS', default=['caivestone.it'])

# https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
SECRET_KEY = env('DJANGO_SECRET_KEY')

# A tuple representing a HTTP header/value combination that signifies a request
# is secure.
# https://docs.djangoproject.com/en/dev/ref/settings/#secure-proxy-ssl-header
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# If True, the SecurityMiddleware redirects all non-HTTPS requests to HTTPS
# (except for those URLs matching a regular expression listed in
# SECURE_REDIRECT_EXEMPT).
# https://docs.djangoproject.com/en/dev/ref/settings/#secure-ssl-redirect
SECURE_SSL_REDIRECT = env.bool('DJANGO_SECURE_SSL_REDIRECT', default=True)

# Whether to use a secure cookie for the session cookie.
# https://docs.djangoproject.com/en/dev/ref/settings/#session-cookie-secure
SESSION_COOKIE_SECURE = True

# Whether to use a secure cookie for the CSRF cookie.
# https://docs.djangoproject.com/en/dev/ref/settings/#csrf-cookie-secure
CSRF_COOKIE_SECURE = True

# If set to a non-zero integer value, the SecurityMiddleware sets the HTTP
# Strict Transport Security header on all responses that do not already have
# it.
# https://docs.djangoproject.com/en/dev/topics/security/#ssl-https
# https://docs.djangoproject.com/en/dev/ref/settings/#secure-hsts-seconds
# TODO: set this to 60 seconds first and then to 518400 once you prove the
#       former works
SECURE_HSTS_SECONDS = 60

# If True, the SecurityMiddleware adds the includeSubDomains directive to the
# HTTP Strict Transport Security header.
# https://docs.djangoproject.com/en/dev/ref/settings/#secure-hsts-include-subdomains
SECURE_HSTS_INCLUDE_SUBDOMAINS = env.bool('DJANGO_SECURE_HSTS_INCLUDE_SUBDOMAINS', default=True)

# If True, the SecurityMiddleware adds the preload directive to the HTTP Strict
# Transport Security header.
# https://docs.djangoproject.com/en/dev/ref/settings/#secure-hsts-preload
SECURE_HSTS_PRELOAD = env.bool('DJANGO_SECURE_HSTS_PRELOAD', default=True)

# If True, the SecurityMiddleware sets the X-Content-Type-Options: nosniff
# header on all responses that do not already have it.
# https://docs.djangoproject.com/en/dev/ref/settings/#secure-content-type-nosniff
SECURE_CONTENT_TYPE_NOSNIFF = env.bool('DJANGO_SECURE_CONTENT_TYPE_NOSNIFF', default=True)


# Cache settings
# -----------------------------------------------------------------------------

# A dictionary containing the settings for all caches to be used with Django.
# Here I use Redis
# https://docs.djangoproject.com/en/dev/ref/settings/#caches
# https://github.com/jazzband/django-redis#configure-as-cache-backend
CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': env('REDIS_URL'),
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
            # Mimicing memcache behavior.
            # https://github.com/jazzband/django-redis#memcached-exceptions-behavior
            'IGNORE_EXCEPTIONS': True,
        },
    }
}


# Logging settings
# https://docs.djangoproject.com/en/dev/topics/logging
# -----------------------------------------------------------------------------

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        }
    },
    'formatters': {
        'verbose': {'format': '%(levelname)s %(asctime)s %(module)s ' '%(process)d %(thread)d %(message)s'}
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },
    'root': {
        'level': 'INFO',
        'handlers': ['console'],
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'django.security.DisallowedHost': {
            'level': 'ERROR',
            'handlers': ['console', 'mail_admins'],
            'propagate': True,
        },
    },
}


# Email settings
# -----------------------------------------------------------------------------

# Default email address to use for various automated correspondence from the
# site manager(s).
# https://docs.djangoproject.com/en/dev/ref/settings/#default-from-email
DEFAULT_FROM_EMAIL = env('DJANGO_DEFAULT_FROM_EMAIL', default='Beppe ' '<noreply@caivestone.it>')

# The email address that error messages come from, such as those sent to ADMINS
# and MANAGERS.
# https://docs.djangoproject.com/en/dev/ref/settings/#server-email
SERVER_EMAIL = env('DJANGO_SERVER_EMAIL', default=DEFAULT_FROM_EMAIL)

# Subject-line prefix for email messages sent with django.core.mail.mail_admins
# or django.core.mail.mail_managers.
# https://docs.djangoproject.com/en/dev/ref/settings/#email-subject-prefix
EMAIL_SUBJECT_PREFIX = env('DJANGO_EMAIL_SUBJECT_PREFIX', default='[Beppe] ')


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/dev/howto/static-files/
# https://docs.djangoproject.com/en/dev/topics/files/
# -----------------------------------------------------------------------------

# The file storage engine to use when collecting static files with the
# collectstatic management command.
# https://docs.djangoproject.com/en/3.1/ref/settings/#staticfiles-storage
# http://whitenoise.evans.io/en/latest/django.html#add-compression-and-caching-support
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'


# Admin settings
# -----------------------------------------------------------------------------

# Django Admin URL regex.
ADMIN_URL = env('DJANGO_ADMIN_URL')
