from typing import Any, List

from cms.sitemaps import CMSSitemap
from djangocms_blog.sitemaps import BlogSitemap

from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.urls import include, path, re_path
from django.views import defaults as default_views

urlpatterns: List[Any] = [
    # Sitemaps
    re_path(
        r'^sitemap\.xml$',
        sitemap,
        {
            'sitemaps': {
                'cmspages': CMSSitemap,
                'blog': BlogSitemap,
            },
        },
    ),
    path('taggit_autosuggest/', include('taggit_autosuggest.urls')),
    # User management
    path('users/', include('beppe.apps.users.urls', namespace='users')),
    path('accounts/', include('allauth.urls')),
]

urlpatterns += i18n_patterns(
    # Django Admin, use {% url 'admin:index' %}
    path(settings.ADMIN_URL, admin.site.urls),
    re_path(r'^', include('cms.urls')),
)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


urlpatterns += [
    # django-filer
    path('filer/', include('filer.urls')),
]

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            '400/',
            default_views.bad_request,
            kwargs={'exception': Exception('Bad Request!')},
        ),
        path(
            '403/',
            default_views.permission_denied,
            kwargs={'exception': Exception('Permission Denied')},
        ),
        path(
            '404/',
            default_views.page_not_found,
            kwargs={'exception': Exception('Page not Found')},
        ),
        path('500/', default_views.server_error),
    ]
    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [path('__debug__/', include(debug_toolbar.urls))] + urlpatterns
