"""
Lodovico form fields
"""

import datetime

from django.core.exceptions import ValidationError
from django.forms.fields import DurationField

from beppe.utils.dateparse import parse_trail_duration
from beppe.utils.duration import trail_duration_string

__all__ = ['TrailDurationField']


class TrailDurationField(DurationField):
    """
    Trail duration field.
    """

    def prepare_value(self, value):
        if isinstance(value, datetime.timedelta):
            return trail_duration_string(value)
        return value

    def to_python(self, value):
        if value in self.empty_values:
            return None
        if isinstance(value, datetime.timedelta):
            return value
        try:
            value = parse_trail_duration(str(value))
        except OverflowError as e:
            raise ValidationError(
                self.error_messages['overflow'].format(
                    min_days=datetime.timedelta.min.days,
                    max_days=datetime.timedelta.max.days,
                ),
                code='overflow',
            ) from e
        if value is None:
            raise ValidationError(self.error_messages['invalid'], code='invalid')
        return value
