"""
Lodovico form widgets
"""

from leaflet import PLUGIN_FORMS, PLUGINS
from leaflet.forms.widgets import LeafletWidget

from django import forms

__all__ = ['ReadonlyTrackWidget']


class ReadonlyTrackWidget(LeafletWidget):
    """
    A Readonly Leaflet widget that says it supports 3d.
    """

    supports_3d = True

    @property
    def media(self):
        """Hide `leaflet.draw controls`."""
        if not self.include_media:
            return forms.Media()

        # We assume that including media for widget means there is
        # no Leaflet at all in the page.
        js = ['leaflet/leaflet.js'] + PLUGINS[PLUGIN_FORMS]['js']
        css = ['leaflet/leaflet.css'] + PLUGINS[PLUGIN_FORMS]['css']

        if self.geom_type == 'LINESTRING':
            css = ['lodovico/css/hide_leaflet_draw.css'] + css

        return forms.Media(js=js, css={'screen': css})
