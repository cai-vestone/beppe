# Generated by Django 3.2.18 on 2023-04-29 08:57

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lodovico', '0011_remove_supplemental'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='trail',
            options={'ordering': ('number', 'suffix', 'variant'), 'verbose_name': 'trail', 'verbose_name_plural': 'trails'},
        ),
        migrations.RemoveConstraint(
            model_name='trail',
            name='unique_number',
        ),
        migrations.AddField(
            model_name='trail',
            name='suffix',
            field=models.CharField(blank=True, max_length=1, validators=[django.core.validators.RegexValidator(message='Enter a letter.', regex='[a-zA-Z]')], verbose_name='suffix'),
        ),
        migrations.AddConstraint(
            model_name='trail',
            constraint=models.UniqueConstraint(fields=('number', 'suffix', 'variant'), name='unique_number'),
        ),
    ]
