# Generated by Django 3.2.13 on 2022-05-07 11:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lodovico', '0009_trail_meta'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='activity',
            options={'ordering': ('name',), 'verbose_name': 'activity', 'verbose_name_plural': 'activities'},
        ),
        migrations.AlterModelOptions(
            name='difficulty',
            options={'ordering': ('weight',), 'verbose_name': 'technical difficulty', 'verbose_name_plural': 'technical difficulties'},
        ),
        migrations.AlterModelOptions(
            name='leg',
            options={'ordering': ('order',), 'verbose_name': 'leg', 'verbose_name_plural': 'legs'},
        ),
        migrations.AlterModelOptions(
            name='trail',
            options={'ordering': ('number',), 'verbose_name': 'trail', 'verbose_name_plural': 'trails'},
        ),
    ]
