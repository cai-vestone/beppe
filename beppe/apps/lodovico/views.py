"""
Lodovico views
"""

from django_downloadview.views import ObjectDownloadView
from django_filters.views import FilterView

from django.views import generic as views

from .filters import TrailFilterSet
from .models import Leg, Trail


class TrailListView(FilterView):
    """
    Trail list.
    """

    model = Trail
    filterset_class = TrailFilterSet
    context_object_name = 'trail_list'
    template_name = 'marta/components/templates/trail_list/trail_list.html'

    def get_queryset(self):
        """Show only published trails."""
        queryset = super().get_queryset()
        return queryset.filter(published=True)


class TrailDetailView(views.DetailView):
    """
    Trail detail
    """

    model = Trail
    context_object_name = 'trail'
    template_name = 'marta/components/templates/trail_detail/trail_detail.html'

    def get_context_data(self, **kwargs):
        """Add instance meta information."""
        kwargs['meta'] = self.get_object().as_meta(self.request)
        return super().get_context_data(**kwargs)


class LegDetailView(views.DetailView):
    """
    Trail leg detail.
    """

    model = Leg
    context_object_name = 'leg'
    template_name = 'marta/components/templates/leg_detail/leg_detail.html'

    def get_context_data(self, **kwargs):
        """Add instance meta information."""
        kwargs['meta'] = self.get_object().as_meta(self.request)
        return super().get_context_data(**kwargs)


class TrackDownloadview(ObjectDownloadView):
    """
    Serve GPX files from tracks.
    """

    file_field = 'track_file'

    def get_basename(self) -> str:
        """Build file name."""
        return f'{self.object}.gpx'

    def get(self, request, *args, **kwargs):
        """Update download count."""
        response = super().get(request, *args, **kwargs)
        self.object.download_count += 1
        self.object.save()
        return response

    def get_file(self):
        """Get the `FieldFile` instance."""
        gpx_file = super().get_file()
        return gpx_file.file
