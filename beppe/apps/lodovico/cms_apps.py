"""
Lodovico django-cms app hooks
"""

from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

from django.utils.translation import gettext_lazy as _

from .urls import urlpatterns


@apphook_pool.register
class LodovicoApphook(CMSApp):
    app_name = 'lodovico'
    name = _('Trails')

    def get_urls(self, page=None, language=None, **kwargs):
        return urlpatterns
