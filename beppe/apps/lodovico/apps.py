"""
Lodovico app config
"""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class LodovicoConfig(AppConfig):
    name = 'beppe.apps.lodovico'
    verbose_name = _('Trails')
