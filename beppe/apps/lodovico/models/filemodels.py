"""
Lodovico filer file models
"""

import os
from datetime import timedelta
from io import UnsupportedOperation

import gpxpy
import srtm
from filer.models.filemodels import File
from gpxpy.gpx import GPXException

from django.contrib.gis.db import models
from django.contrib.gis.geos import LineString, MultiLineString, Point
from django.utils.translation import gettext_lazy as _

from .mixins import TrackStatistics

__all__ = ['GpxFile']


class GpxFile(TrackStatistics, File):
    """
    A GPX file.
    """

    track_data = models.MultiLineStringField(
        _('preview'),
        dim=3,
        srid=4326,  # same as in GPX files
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name = _('GPX file')
        verbose_name_plural = _('GPX files')

    def save(self, *args, **kwargs):
        """Fill `track_data` and add elevation data if necessary."""
        gpx = self.parse_gpx()
        if gpx:
            self.fill_stats(gpx)
        super().save(*args, **kwargs)

    def parse_gpx(self):
        """
        Parse the GPX file, fill ``track_data``, add elevation data if
        necessary and kill all waypoints.
        """
        if self.file:
            dirty = False
            # TODO: check file encoding
            gpx_text = self.file.read().decode('utf-8')
            try:
                gpx = gpxpy.parse(gpx_text)
                elevation_data = srtm.get_data()
                if not gpx.has_elevations():
                    # add elevations
                    elevation_data.add_elevations(gpx, smooth=True)
                    dirty = True
                if gpx.waypoints:
                    gpx.waypoints = []
                    dirty = True
                if gpx.tracks:
                    tracks = []
                    for track in gpx.tracks:
                        track_points = []
                        for segment in track.segments:
                            for point in segment.points:
                                if point.elevation is None:
                                    point.elevation = elevation_data.get_elevation(point.latitude, point.longitude)
                                    dirty = True
                                track_points.append(
                                    Point(
                                        point.longitude,
                                        point.latitude,
                                        point.elevation,
                                    )
                                )
                        tracks.append(LineString(track_points))
                    self.track_data = MultiLineString(tracks)
                if dirty:
                    gpx_file = self.file.open('w')
                    gpx_text = gpx.to_xml() + '\n'
                    try:
                        gpx_file.write(gpx_text.encode('utf-8'))
                        gpx_file.truncate()
                        # gpx_file.close()  # ValueError: I/O operation on closed file.
                    except UnsupportedOperation:
                        # FIXME: don't ignore exceptions
                        pass
                return gpx
            except GPXException:
                # ignore parsing errors
                pass
        return None

    def fill_stats(self, gpx):
        """Fill the statistics fields."""
        drop_data = gpx.get_uphill_downhill()
        moving_data = gpx.get_moving_data()
        self.distance = gpx.length_3d()
        self.positive_drop = drop_data.uphill
        self.negative_drop = drop_data.downhill
        self.duration = timedelta(seconds=moving_data.moving_time)

    @classmethod
    def matches_file_type(cls, iname, ifile, mime_type) -> bool:
        filename_extensions = ['.gpx']
        ext = os.path.splitext(iname)[1].lower()
        return ext in filename_extensions
