"""
Lodovico filer model fields
"""

from filer.fields.file import FilerFileField

from beppe.apps.lodovico.models import GpxFile

__all__ = ['FilerGpxField']


class FilerGpxField(FilerFileField):
    """
    A GPX file field.
    """

    default_model_class = GpxFile
