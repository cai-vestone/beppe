"""
Lodovico trail model fields
"""

import datetime

from django.core.exceptions import ValidationError
from django.db.models import fields

from beppe.apps.lodovico import forms
from beppe.utils.dateparse import parse_trail_duration
from beppe.utils.duration import trail_duration_string

__all__ = ['TrailDurationField']


class TrailDurationField(fields.DurationField):
    """
    Trail duration model field.
    """

    def to_python(self, value):
        if value is None:
            return value
        if isinstance(value, datetime.timedelta):
            return value
        try:
            parsed = parse_trail_duration(value)
        except ValueError:
            pass
        else:
            if parsed is not None:
                return parsed

        raise ValidationError(
            self.error_messages['invalid'],
            code='invalid',
            params={'value': value},
        )

    def value_to_string(self, obj):
        val = self.value_from_object(obj)
        return '' if val is None else trail_duration_string(val)

    def formfield(self, **kwargs):
        return super().formfield(
            **{
                'form_class': forms.TrailDurationField,
                **kwargs,
            }
        )
