"""
Difficulty models
"""

from django.db import models
from django.utils.translation import gettext_lazy as _

__all__ = [
    'Activity',
    'Difficulty',
]


class Activity(models.Model):
    """
    Activity.
    """

    name = models.CharField(_('name'), max_length=200)

    class Meta:
        ordering = ('name',)
        verbose_name = _('activity')
        verbose_name_plural = _('activities')

    def __str__(self):
        """Use `name`."""
        return self.name


class Difficulty(models.Model):
    """
    Technical difficulty of a trail.
    """

    activity = models.ForeignKey(
        Activity,
        on_delete=models.CASCADE,
        verbose_name=_('activity'),
        related_name='difficulties',
        related_query_name='difficulty',
    )

    name = models.CharField(_('name'), max_length=200)

    description = models.TextField(_('description'), blank=True)

    label = models.CharField(
        _('label'),
        max_length=10,
        primary_key=True,
    )

    weight = models.PositiveIntegerField(_('weight'), default=0)

    class Meta:
        ordering = ('weight',)
        verbose_name = _('technical difficulty')
        verbose_name_plural = _('technical difficulties')

    def __str__(self):
        return f'{self.label}: {self.name}'
