"""
Lodovico model mixins
"""

from typing import Optional

from django.contrib import admin
from django.db import models
from django.utils import formats
from django.utils.translation import gettext_lazy as _

from beppe.utils.duration import _get_duration_components, trail_duration_string

from .fields.trail import TrailDurationField


class TrackStatistics(models.Model):
    """
    Some common track statistics.
    """

    duration = TrailDurationField(
        _('duration'),
        null=True,
        blank=True,
        help_text=_('Format: "hours:minutes".'),
    )

    distance = models.PositiveIntegerField(_('distance'), null=True, blank=True, help_text=_('In metres.'))

    positive_drop = models.PositiveIntegerField(_('positive drop'), null=True, blank=True, help_text=_('In metres.'))

    negative_drop = models.PositiveIntegerField(_('negative drop'), null=True, blank=True, help_text=_('In metres.'))

    class Meta:
        abstract = True

    @property
    @admin.display(description=_('duration'))
    def duration_label(self) -> Optional[str]:
        """Human-readable duration."""
        if self.duration is None:
            return None
        hours, minutes = _get_duration_components(self.duration)
        if hours:
            return trail_duration_string(self.duration)
        return _('{value} minutes').format(value=minutes)

    @property
    @admin.display(description=_('distance'))
    def distance_label(self) -> Optional[str]:
        """Human-readable distance (in km)"""
        if self.distance is None:
            return None
        value = self.distance / 1000
        return _('{value} km').format(value=formats.number_format(value, 2))

    @property
    @admin.display(description=_('positive drop'))
    def positive_drop_label(self) -> Optional[str]:
        """Human-readable positive drop (in m)"""
        if self.positive_drop is None:
            return None
        return _('{value} m').format(value=formats.number_format(self.positive_drop, 0))

    @property
    @admin.display(description=_('negative drop'))
    def negative_drop_label(self) -> Optional[str]:
        """Human-readable negative drop (in m)."""
        if self.negative_drop is None:
            return None
        return _('{value} m').format(value=formats.number_format(self.negative_drop, 0))
