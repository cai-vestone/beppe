"""
Lodovico abstract base models
"""

from datetime import timedelta

from django.conf import settings
from django.contrib import admin
from django.contrib.gis.geos import Point
from django.db import models
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _

from .difficulty import Difficulty
from .fields.filer import FilerGpxField
from .mixins import TrackStatistics


def walking_time(distance, drop):
    """Calculate a walking time given distance and elevation gain."""
    slope = -(drop / distance * 100)
    if slope < -10:
        x = sum([0.0023 * slope**2, -0.6665 * slope, 9.664])
    elif slope < 10:
        x = sum(
            [
                0.00002 * slope**4,
                -0.0013 * slope**3,
                0.0307 * slope**2,
                0.3729 * slope,
                14.123,
            ]
        )
    else:
        x = sum([-0.0002 * slope**2, 1.5041 * slope, 2.1252])
    return timedelta(minutes=int(x * distance / 1000))


class BaseTrail(TrackStatistics, models.Model):
    """
    Abstract base for everything trail-like.
    """

    difficulty = models.ForeignKey(
        Difficulty,
        on_delete=models.SET_NULL,
        verbose_name=_('difficulty'),
        default=None,
        null=True,
        blank=True,
        limit_choices_to={'activity_id': settings.LODOVICO_TRAIL_ACTIVITY_ID},
    )

    track_file = FilerGpxField(on_delete=models.CASCADE, verbose_name=_('track file'))

    download_count = models.PositiveIntegerField(_('download count'), default=0)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        """Fill in track statistics if necessary."""
        if self.track_file:
            if not self.distance:
                self.distance = self.track_file.distance
            if not self.positive_drop:
                self.positive_drop = self.track_file.positive_drop
            if not self.negative_drop:
                self.negative_drop = self.track_file.negative_drop
            if not self.duration:
                self.duration = walking_time(self.distance, self.positive_drop)
        super().save(*args, **kwargs)

    @cached_property
    @admin.display(description=_('track'))
    def track_data(self):
        """Track data."""
        return self.track_file.track_data.merged

    @cached_property
    @admin.display(description=_('start'))
    def start(self) -> Point:
        """Track starting point"""
        return Point(self.track_data[0])

    @cached_property
    @admin.display(description=_('arrival'))
    def end(self) -> Point:
        """Track arrival point."""
        return Point(self.track_data[-1])
