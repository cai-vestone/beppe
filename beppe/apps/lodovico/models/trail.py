"""
Trail models
"""

from typing import Optional

from cms.models.fields import PlaceholderField
from filer.fields.image import FilerImageField

from django.contrib import admin
from django.core.validators import RegexValidator
from django.db import models
from django.db.models import UniqueConstraint
from django.db.models.functions import Concat
from django.urls import NoReverseMatch, reverse
from django.utils.encoding import force_str
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _
from django.utils.translation import pgettext

from beppe.models.meta import BeppeModelMeta

from .base import BaseTrail

__all__ = [
    'Leg',
    'Trail',
]


class Trail(BeppeModelMeta, BaseTrail):
    """
    Numbered trail.
    """

    number = models.PositiveIntegerField(_('number'))

    suffix = models.CharField(
        _('suffix'),
        max_length=1,
        blank=True,
        validators=[
            RegexValidator(
                regex=r'[a-zA-Z]',
                message=_('Enter a letter.'),
            )
        ],
    )

    variant = models.BooleanField(_('variant'), default=False)

    published = models.BooleanField(pgettext('masculine', 'published'), blank=True, default=False)

    name = models.CharField(_('name'), max_length=200)

    description = PlaceholderField(
        'trail_description',
        related_name='descripted_trail',
        related_query_name='descripted_trail',
    )

    header_background = PlaceholderField('trail_header')

    meta_description = models.CharField(_('description'), max_length=2000, blank=True, default="")

    meta_image = FilerImageField(
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='+',
        verbose_name=_('picture'),
    )

    class Meta:
        ordering = ('number', 'suffix', 'variant')
        verbose_name = _('trail')
        verbose_name_plural = _('trails')
        constraints = [
            UniqueConstraint(fields=['number', 'suffix', 'variant'], name='unique_number'),
        ]

    def __str__(self):
        """<number>. <name>"""
        return f'{self.number_label}. {self.name}'

    def get_absolute_url(self) -> Optional[str]:
        try:
            return reverse('lodovico:trail_detail', kwargs={'pk': self.pk})
        except NoReverseMatch:
            return None

    def clean_fields(self, exclude) -> None:
        """Normalize suffix."""
        super().clean_fields(exclude)
        exclude = exclude or []
        if 'suffix' not in exclude:
            self.suffix = self.suffix.lower()

    @property
    @admin.display(description=_('number'), ordering=Concat('number', 'suffix', 'variant'))
    def number_label(self) -> str:
        label = f'{self.number}{self.suffix}'
        if self.variant:
            suffix = _('var')
            label = f'{label} {suffix}'
        return label

    @property
    @admin.display(description=_('download url'))
    def download_url(self) -> str:
        """GPX file download URL."""
        return reverse('lodovico:trail_download', kwargs={'pk': self.pk})

    def get_meta_title(self) -> str:
        return str(self)

    def get_meta_description(self) -> Optional[str]:
        """Use `self.meta_description` when set, use current page as fallback."""
        return self.meta_description or super().get_meta_description()

    def get_meta_image(self):
        """Use `self.meta_image` when set, use current page as fallback."""
        return self.meta_image or super().get_meta_image()


class Leg(BeppeModelMeta, BaseTrail):
    """
    Leg of a trail.
    """

    trail = models.ForeignKey(
        Trail,
        on_delete=models.CASCADE,
        verbose_name=_('trail'),
        related_name='legs',
        related_query_name='leg',
    )

    name = models.CharField(_('name'), max_length=200, blank=True)

    description = PlaceholderField(
        'leg_description',
        related_name='descripted_legs',
        related_query_name='descripted_leg',
    )

    header_background = PlaceholderField('leg_header')

    order = models.PositiveIntegerField(_('weight'), default=0)

    class Meta:
        ordering = ('order',)
        verbose_name = _('leg')
        verbose_name_plural = _('legs')

    def __str__(self):
        """Use name or index."""
        if self.name:
            return force_str(self.name)
        index = self.index
        if index:
            return _('Leg {number}').format(number=index)
        return force_str(_('Leg'))

    def get_absolute_url(self) -> Optional[str]:
        try:
            return reverse(
                'lodovico:leg_detail',
                kwargs={
                    'trail_pk': self.trail.pk,
                    'pk': self.pk,
                },
            )
        except NoReverseMatch:
            return None

    def get_meta_title(self) -> str:
        return f'{self.trail} / {self}'

    def get_meta_description(self) -> Optional[str]:
        """Use the parent trail."""
        return self.trail.get_meta_description()

    def get_meta_image(self):
        """Use the parent trail."""
        return self.trail.get_meta_image()

    @cached_property
    @admin.display(description=_('index'))
    def index(self) -> Optional[int]:
        """1-based index of this leg in its trail."""
        if self.pk:
            return list(self.trail.legs.all()).index(self) + 1
        return None

    @cached_property
    @admin.display(description=_('download url'))
    def download_url(self) -> str:
        """GPX file download URL."""
        return reverse('lodovico:leg_download', kwargs={'trail_pk': self.trail.pk, 'pk': self.pk})
