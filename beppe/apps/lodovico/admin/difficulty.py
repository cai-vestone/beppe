"""
Difficulty admin interface
"""

from adminsortable2.admin import SortableAdminMixin, SortableInlineAdminMixin

from django.contrib import admin

from beppe.apps.lodovico import models

__all__ = [
    'ActivityAdmin',
    'DifficultyAdmin',
]


@admin.register(models.Difficulty)
class DifficultyAdmin(admin.ModelAdmin):
    """
    Difficulty admin interface.
    """

    list_display = (
        'label',
        'name',
        'activity',
    )
    list_filter = ('activity',)
    search_fields = ('label', 'name')
    fields = (
        'activity',
        'label',
        'name',
        'description',
    )


class DifficultyInline(SortableInlineAdminMixin, admin.TabularInline):
    """
    Difficulty inline for `ActivityAdmin`.
    """

    model = models.Difficulty
    fields = (
        'weight',
        'label',
        'name',
    )
    extra = 0
    can_delete = False
    show_change_link = True


@admin.register(models.Activity)
class ActivityAdmin(SortableAdminMixin, admin.ModelAdmin):
    """
    Activity admin interface.
    """

    list_display = fields = ('name',)
    inlines = (DifficultyInline,)
