"""
Lodovico admin classes
"""

from .difficulty import *  # noqa F403
from .difficulty import __all__ as difficulty_all
from .filer import *  # noqa F403
from .filer import __all__ as filer_all
from .trail import *  # noqa F403
from .trail import __all__ as trail_all

__all__ = []
__all__ += difficulty_all
__all__ += filer_all
__all__ += trail_all
