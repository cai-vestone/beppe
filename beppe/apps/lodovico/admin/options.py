"""
Lodovico admin options
"""

from leaflet import admin as leaflet

from django.contrib import admin
from django.contrib.admin.options import TO_FIELD_VAR
from django.contrib.admin.templatetags.admin_urls import add_preserved_filters
from django.contrib.admin.utils import unquote
from django.http import HttpResponseRedirect
from django.urls import reverse

from beppe.apps.lodovico.forms.widgets import ReadonlyTrackWidget

__all__ = [
    'LeafletGeoAdmin',
    'LeafletGeoAdminMixin',
    'ChildModelAdmin',
]


class LeafletGeoAdmin(leaflet.LeafletGeoAdmin):
    """
    Substitute the default widget for leaflet fields.
    """

    widget = ReadonlyTrackWidget


class LeafletGeoAdminMixin(leaflet.LeafletGeoAdminMixin):
    """
    Substitute the default widget for leaflet fields.
    """

    widget = ReadonlyTrackWidget


class TwoFacedModelAdmin(admin.ModelAdmin):
    """
    Use different fieldsets for add and change views.
    """

    add_fieldsets = None

    def get_fieldsets(self, request, obj=None):
        if obj is None and self.add_fieldsets:
            return self.add_fieldsets
        return super().get_fieldsets(request, obj)


class ChildModelAdmin(admin.ModelAdmin):
    def has_module_permission(self, request):
        """Hide yourself in module indexes."""
        return False

    def get_extra_context(self, request, obj):
        """
        Get extra context data for `change_view`, `delete_view` and `history_view`.
        """
        parent = self._get_parent_instance(request, obj)
        return {
            'parent': {
                'instance': parent,
                'opts': parent._meta,
            },
        }

    def _get_parent_instance(self, request, obj):
        raise NotImplementedError('{klass:s}._get_parent_instance'.format(klass=self.__class__))

    def _response_post_save(self, request, obj):
        parent = self._get_parent_instance(request, obj)
        opts = parent._meta

        # TODO: check permissions (view or change) on parent
        post_url = reverse(
            f'admin:{opts.app_label:s}_{opts.model_name:s}_change',
            args=[parent.pk],
            current_app=self.admin_site.name,
        )
        preserved_filters = self.get_preserved_filters(request)
        post_url = add_preserved_filters({'preserved_filters': preserved_filters, 'opts': opts}, post_url)
        return HttpResponseRedirect(post_url)

    def _get_instance(self, request, object_id):
        to_field = request.POST.get(TO_FIELD_VAR, request.GET.get(TO_FIELD_VAR))
        return self.get_object(request, unquote(object_id), to_field)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        instance = self._get_instance(request, object_id)
        extra_context.update(self.get_extra_context(request, instance))
        return super().change_view(request, object_id, form_url, extra_context)

    def delete_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        instance = self._get_instance(request, object_id)
        extra_context.update(self.get_extra_context(request, instance))
        return super().delete_view(request, object_id, extra_context)

    def history_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        instance = self._get_instance(request, object_id)
        extra_context.update(self.get_extra_context(request, instance))
        return super().history_view(request, object_id, extra_context)
