"""
Trail admin interface
"""

from adminsortable2.admin import SortableAdminMixin, SortableInlineAdminMixin
from cms.admin.placeholderadmin import FrontendEditableAdminMixin, PlaceholderAdminMixin

from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from beppe.apps.lodovico import models

from .options import ChildModelAdmin, TwoFacedModelAdmin

__all__ = [
    'TrailAdmin',
    'LegAdmin',
]


class LegInline(SortableInlineAdminMixin, admin.TabularInline):
    """
    Leg inline for `TrailAdmin`.
    """

    model = models.Leg
    fields = (
        'order',
        'track_file',
        'name',
        'download_count',
    )
    readonly_fields = ('download_count',)
    extra = 0
    show_change_link = True

    # class Media:
    #     css = {'all': ('lodovico/css/tabularinline-mute.css',)}


class TrackAdminMixin(PlaceholderAdminMixin, FrontendEditableAdminMixin):
    """
    Base track admin mixin.
    """

    frontend_editable_fields = (
        'name',
        'difficulty',
        'duration',
        'distance',
        'positive_drop',
        'negative_drop',
    )


@admin.register(models.Trail)
class TrailAdmin(SortableAdminMixin, TrackAdminMixin, TwoFacedModelAdmin):
    """
    Trail admin interface.
    """

    list_display = (
        'number_label',
        'name',
        'difficulty',
        'download_count',
        'published',
    )
    list_filter = (
        'published',
        'difficulty',
    )
    search_fields = (
        'number',
        'name',
    )
    add_fieldsets = (
        (  # type: ignore
            None,
            {
                'fields': (
                    'variant',
                    'number',
                    'suffix',
                    'name',
                    'track_file',
                ),
            },
        ),
        (
            _('Meta-information'),
            {
                'fields': (
                    'meta_image',
                    'meta_description',
                ),
            },
        ),
    )
    fieldsets = (
        (
            None,
            {
                'fields': (
                    'published',
                    'variant',
                    'number',
                    'suffix',
                    'name',
                    'track_file',
                ),
            },
        ),
        (
            _('Statistics'),
            {
                'fields': (
                    'difficulty',
                    'duration',
                    'distance',
                    'positive_drop',
                    'negative_drop',
                ),
            },
        ),
        (
            _('Meta-information'),
            {
                'fields': (
                    'meta_image',
                    'meta_description',
                ),
            },
        ),
    )
    inlines = (LegInline,)

    def get_inlines(self, request, obj=None):
        """Don't use inlines in the add form."""
        if obj is None:
            return ()
        return super().get_inlines(request, obj)


@admin.register(models.Leg)
class LegAdmin(TrackAdminMixin, ChildModelAdmin):
    """
    Admin for trek legs.
    """

    fieldsets = (
        (
            None,
            {
                'fields': (
                    'name',
                    'track_file',
                ),
            },
        ),
        (
            _('Statistics'),
            {
                'fields': (
                    'difficulty',
                    'duration',
                    'distance',
                    'positive_drop',
                    'negative_drop',
                ),
            },
        ),
    )

    def _get_parent_instance(self, request, obj):
        """A leg parent is its trail."""
        return obj.trail
