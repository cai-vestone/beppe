"""
Lodovico filer file admin classes
"""

from filer.admin.fileadmin import FileAdmin

from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from beppe.apps.lodovico import models

from .options import LeafletGeoAdminMixin

__all__ = ['GpxFileAdmin']


@admin.register(models.GpxFile)
class GpxFileAdmin(LeafletGeoAdminMixin, FileAdmin):
    """
    GPX file filer admin.
    """

    def get_readonly_fields(self, request, obj=None):
        """Add track statistics."""
        fields = super().get_readonly_fields(request, obj)
        return list(fields) + [
            'duration_label',
            'distance_label',
            'positive_drop_label',
            'negative_drop_label',
        ]

    def get_fieldsets(self, request, obj=None):
        """Add a leaflet preview."""
        fieldsets = super().get_fieldsets(request, obj)
        if obj and obj.track_data:
            fieldsets = (
                (
                    None,
                    {
                        'fields': ('track_data',),
                    },
                ),
                (
                    _('Statistics'),
                    {
                        'fields': (
                            'duration_label',
                            'distance_label',
                            'positive_drop_label',
                            'negative_drop_label',
                        ),
                    },
                ),
            ) + fieldsets
        return fieldsets
