"""
Lodovico filters
"""

import django_filters as filters

from django.conf import settings
from django.utils.translation import gettext_lazy as _

from .forms.fields import TrailDurationField
from .models import Difficulty, Trail


class TrailDurationFilter(filters.DurationFilter):
    """
    A `DurationFilter` using a `TrailDurationField` form field.
    """

    field_class = TrailDurationField


class TrailFilterSet(filters.FilterSet):
    """
    Filter trails on difficulty and duration.
    """

    difficulty = filters.ModelChoiceFilter(
        queryset=Difficulty._default_manager.filter(activity__pk=settings.LODOVICO_TRAIL_ACTIVITY_ID),
        field_name='difficulty',
        lookup_expr='weight__lte',
        label=_('Maximum difficulty'),
        method='filter_difficulty',
    )

    duration = TrailDurationFilter(
        field_name='duration',
        lookup_expr='lte',
        label=_('Maximum duration hours'),
    )

    def filter_difficulty(self, queryset, name, value):
        return queryset.filter(difficulty__weight__lte=value.weight)

    class Meta:
        model = Trail
        fields = ('difficulty', 'duration')
