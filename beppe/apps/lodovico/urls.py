"""
Lodovico root urlconf
"""

from django.urls import path

from . import models, views

urlpatterns = [
    path('<int:trail_pk>/<int:pk>/', views.LegDetailView.as_view(), name='leg_detail'),
    path(
        '<int:trail_pk>/<int:pk>/download/',
        views.TrackDownloadview.as_view(model=models.Leg),
        name='leg_download',
    ),
    path('<int:pk>/', views.TrailDetailView.as_view(), name='trail_detail'),
    path(
        '<int:pk>/download/',
        views.TrackDownloadview.as_view(model=models.Trail),
        name='trail_download',
    ),
    path('', views.TrailListView.as_view(), name='trail_list'),
]
