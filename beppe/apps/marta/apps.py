"""
Marta app config
"""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class MartaConfig(AppConfig):
    name = 'beppe.apps.marta'
    verbose_name = _('Marta')
