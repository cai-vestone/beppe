"""
Marta template tags and filters
"""

from djangocms_page_meta.templatetags.page_meta_tags import MetaFromPage
from easy_thumbnails.files import get_thumbnailer

from django import template

from beppe.utils.meta import get_page_meta_image

register = template.Library()


@register.tag(name='page_meta')
class MartaMetaFromPage(MetaFromPage):
    def render_tag(self, context, page, varname):
        """Crop image."""
        result = super().render_tag(context, page, varname)
        request = context.get('request')
        if request:
            meta = context.get(varname)
            image = get_page_meta_image(request, page)
            if image is not None:
                thumbnailer = get_thumbnailer(image)
                meta.image = thumbnailer['meta_image'].url
        return result


@register.filter('post_meta')
def post_meta(post):
    meta = post.as_meta()
    if post.main_image:
        thumbnailer = get_thumbnailer(post.main_image)
        meta.image = thumbnailer['meta_image'].url
    return meta
