/**
 * Polaroid
 */

window.addEventListener('DOMContentLoaded', (event) => {
  function disablePolaroidLinks() {
    document.querySelectorAll('.bp-c-polaroid').forEach((polaroid) => {
      polaroid.addEventListener('click', (event) => event.preventDefault());
    });
  }

  disablePolaroidLinks();
  CMS.$(window).on('cms-content-refresh', function () {
    disablePolaroidLinks();
  });
});
