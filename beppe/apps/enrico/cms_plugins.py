"""
Enrico django-cms plugins
"""

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from django.utils.text import capfirst
from django.utils.translation import gettext_lazy as _

from . import models


@plugin_pool.register_plugin
class TripSchedulePlugin(CMSPluginBase):
    """
    Trip schedule plugin.
    """

    name = capfirst(models.TripSchedule._meta.verbose_name)
    module = _('Trips')
    model = models.TripSchedulePluginModel
    render_template = 'enrico/trip_schedule.html'
    fields = (
        'title',
        'schedule',
        'kinds',
        'show_past',
    )
    filter_horizontal = ('kinds',)

    def render(self, context, instance, placeholder):
        additional_context = {
            'title': instance.title,
            'trips': instance.get_trips(),
        }
        context.update(additional_context)

        return super().render(context, instance, placeholder)


@plugin_pool.register_plugin
class NextTripPlugin(CMSPluginBase):
    """
    Next trip plugin.
    """

    name = _('Next scheduled trip')
    module = _('Trips')
    model = models.NextTripPluginModel
    render_template = 'enrico/next_trip.html'
    fields = (
        'title',
        'kinds',
    )
    filter_horizontal = ('kinds',)

    def render(self, context, instance, placeholder):
        additional_context = {
            'title': instance.title,
            'trip': instance.get_trip(),
        }
        context.update(additional_context)

        return super().render(context, instance, placeholder)
