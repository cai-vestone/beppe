"""
Enrico django-cms plugin models
"""

from typing import TYPE_CHECKING, Optional

from cms.models import CMSPlugin

from django.db import models
from django.utils import timezone
from django.utils.text import capfirst
from django.utils.translation import gettext_lazy as _

from . import trip

if TYPE_CHECKING:
    from django.db.models import QuerySet

__all__ = [
    'NextTripPluginModel',
    'TripSchedulePluginModel',
]


class KindedPluginModel(CMSPlugin):
    """
    A CMS plugin related to trip kinds.
    """

    kinds = models.ManyToManyField(
        trip.TripKind,
        verbose_name=trip.TripKind._meta.verbose_name_plural or None,
    )

    class Meta:
        abstract = True

    def copy_relations(self, old_instance: 'KindedPluginModel'):
        """Copy trip kinds."""
        self.kinds.set(old_instance.kinds.all())


class TripSchedulePluginModel(KindedPluginModel):
    """
    Trip schedule plugin model.
    """

    title = models.CharField(
        _('title'),
        max_length=200,
        default=capfirst(trip.TripSchedule._meta.verbose_name),
    )

    schedule = models.ForeignKey(
        trip.TripSchedule,
        on_delete=models.CASCADE,
        verbose_name=_('schedule'),
        related_name='+',
    )

    show_past = models.BooleanField(_('show past trips'), default=False)

    class Meta:
        verbose_name = _('trip schedule plugin')
        verbose_name_plural = _('trip schedule plugins')

    def __str__(self):
        return str(self.schedule.year)

    def get_trips(self) -> 'QuerySet[trip.Trip]':
        """Get the trips to display."""
        queryset = self.schedule.trips.filter(kind__in=self.kinds.all()).order_by('scheduled_date')
        if not self.show_past:
            queryset = queryset.filter(scheduled_date__gte=timezone.now())
        return queryset


class NextTripPluginModel(KindedPluginModel):
    """
    Next trip plugin.
    """

    title = models.CharField(_('title'), max_length=200, default=_('Next scheduled trip'))

    class Meta:
        verbose_name = _('next trip plugin')
        verbose_name_plural = _('next trip plugins')

    def __str__(self):
        """Use `title`."""
        return self.title

    def get_trip(self) -> Optional[trip.Trip]:
        """Get the trip to display."""
        candidate = trip.Trip.objects.get_next()
        if candidate is not None and candidate.kind in self.kinds.all():
            return candidate
        return None
