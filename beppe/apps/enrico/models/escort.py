"""
Enrico escort models
"""

from django.contrib import admin
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Value
from django.db.models.functions import Concat
from django.utils.translation import gettext_lazy as _

__all__ = [
    'Escort',
    'EscortRole',
]

User = get_user_model()


class Escort(models.Model):
    """
    Escort.
    """

    user = models.OneToOneField(
        User,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name=_('user'),
        related_name='escort',
    )

    is_group = models.BooleanField(_('group'), default=False)

    first_name = models.CharField(_('first name'), max_length=150)

    last_name = models.CharField(_('last name'), max_length=150, blank=True)

    email = models.EmailField(_('email address'), blank=True)

    telephone = models.CharField(_('telephone number'), max_length=20, blank=True)

    class Meta:
        ordering = ('first_name', 'last_name')
        verbose_name = _('escort')
        verbose_name_plural = _('escorts')

    def __str__(self):
        if self.is_group:
            return self.first_name
        return self.full_name

    def clean_fields(self, exclude=None):
        """Last name can only be omitted for groups."""
        exclude = exclude or []
        if not self.last_name and 'last_name' not in exclude:
            if not self.is_group:
                raise ValidationError({'last_name': _('This field is required.')})

    @property
    @admin.display(
        description=_('full name'),
        ordering=Concat('first_name', Value(' '), 'last_name'),
    )
    def full_name(self) -> str:
        """First and last name."""
        return f'{self.first_name} {self.last_name}'


class EscortRole(models.Model):
    """
    An escort role.
    """

    name = models.CharField(_('name'), max_length=200)

    class Meta:
        ordering = ('name',)
        verbose_name = _('escort role')
        verbose_name_plural = _('escort roles')

    def __str__(self):
        """Use `name`."""
        return self.name
