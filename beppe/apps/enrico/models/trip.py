"""
Enrico trip models
"""

from datetime import date, timedelta
from typing import TYPE_CHECKING, Optional, Tuple

from cms.models.fields import PlaceholderField
from easy_thumbnails.files import get_thumbnailer
from filer.fields.image import FilerFileField, FilerImageField

from django.contrib import admin
from django.contrib.gis.geos import Point
from django.db import models
from django.db.models import UniqueConstraint
from django.urls import reverse
from django.utils import formats
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _
from django.utils.translation import pgettext_lazy

from beppe.apps.enrico.models.escort import Escort, EscortRole
from beppe.apps.lodovico.models import Difficulty, Trail
from beppe.apps.lodovico.models.fields.filer import FilerGpxField
from beppe.models.meta import BeppeModelMeta

if TYPE_CHECKING:
    from django.db.models.manager import RelatedManager

__all__ = [
    'EscortAttendance',
    'TripKind',
    'TripSchedule',
    'Trip',
]


class TripKind(models.Model):
    """
    Trip kind.
    """

    name = models.CharField(_('name'), max_length=200)

    order = models.PositiveSmallIntegerField(_('order'), default=0)

    class Meta:
        ordering = ('order',)
        verbose_name = _('trip kind')
        verbose_name_plural = _('trip kinds')

    def __str__(self):
        """Use `name`."""
        return self.name


class TripScheduleManager(models.Manager):
    """
    Trip schedule manager.
    """

    def get_by_natural_key(self, year: int):
        return self.get(year=year)


class TripSchedule(models.Model):
    """
    Trip schedule.
    """

    if TYPE_CHECKING:
        # FIXME: why do I need to do this?
        trips: 'RelatedManager[Trip]'

    year = models.PositiveSmallIntegerField(_('year'), unique=True)

    name = models.CharField(_('name'), max_length=200, blank=True)

    objects = TripScheduleManager()

    class Meta:
        ordering = ('year', 'name')
        verbose_name = _('trip schedule')
        verbose_name_plural = _('trip schedules')

    def natural_key(self) -> Tuple[int]:
        return (self.year,)

    def __str__(self):
        """Use `name` if present, `year` otherwise."""
        if self.name:
            return self.name
        return _('{year} schedule').format(year=self.year)

    def get_absolute_url(self):
        return reverse(
            'enrico:tripschedule_detail',
            kwargs={
                'year': self.year,
            },
        )

    @admin.display(description=_('start date'))
    def start_date(self) -> Optional[date]:
        """Start date."""
        if self.trips.exists():
            return self.trips.earliest('scheduled_date').scheduled_date
        return None

    @admin.display(description=pgettext_lazy('schedule', 'end date'))
    def end_date(self) -> Optional[date]:
        """End date."""
        if self.trips.exists():
            return self.trips.latest('scheduled_date').scheduled_date
        return None


class TripManager(models.Manager):
    """
    Manager for trips.
    """

    def get_next(self) -> 'Optional[Trip]':
        """Get the next scheduled trip."""
        return self.filter(canceled=False, scheduled_date__gte=date.today()).order_by('scheduled_date').first()

    def get_by_natural_key(self, year: int, slug: str):
        return self.get(schedule__year=year, slug=slug)


class Trip(BeppeModelMeta, models.Model):
    """
    A Trip.
    """

    scheduled_date = models.DateField(_('scheduled date'))

    canceled = models.BooleanField(pgettext_lazy('trip', 'canceled'), default=False)

    duration = models.PositiveSmallIntegerField(_('duration'), default=1, help_text=_('In days.'))

    name = models.CharField(_('name'), max_length=200)

    slug = models.SlugField(_('slug'), max_length=200)

    flyer = FilerFileField(
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name=_('flyer'),
        related_name='+',
    )

    flyer_picture = FilerImageField(
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name=_('flyer picture'),
        related_name='+',
    )

    escorts = models.ManyToManyField(
        Escort,
        through='EscortAttendance',
        verbose_name=_('escorts'),
        related_name='trips',
        related_query_name='trip',
    )

    difficulty = models.ForeignKey(
        Difficulty,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        default=None,
        verbose_name=_('difficulty'),
        related_name='trips',
        related_query_name='trip',
    )

    schedule = models.ForeignKey(
        TripSchedule,
        on_delete=models.CASCADE,
        verbose_name=_('schedule'),
        related_name='trips',
        related_query_name='trip',
    )

    kind = models.ForeignKey(
        TripKind,
        on_delete=models.CASCADE,
        verbose_name=_('kind'),
        related_name='trips',
        related_query_name='trip',
    )

    related_trails = models.ManyToManyField(
        Trail,
        verbose_name=_('related trails'),
        related_name='trips',
        related_query_name='trip',
    )

    header_background = PlaceholderField('trip_header')

    report = PlaceholderField(
        'trip_report',
        verbose_name=_('report'),
        related_name='report_trips',
        related_query_name='report_trip',
    )

    pictures = PlaceholderField(
        'trip_pictures',
        verbose_name=_('pictures'),
        related_name='pictures_trips',
        related_query_name='pictures_trip',
    )

    track_file = FilerGpxField(
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name=_('track file'),
        related_name='trips',
        related_query_name='trip',
    )

    objects = TripManager()

    class Meta:
        ordering = ('scheduled_date', 'name')
        verbose_name = _('trip')
        verbose_name_plural = _('trips')
        constraints = [
            models.UniqueConstraint(
                fields=['schedule', 'slug'],
                name='trip_unique_slug',
            )
        ]

    def natural_key(self) -> Tuple[int, str]:
        return (self.schedule.year, self.slug)

    def __str__(self):
        """Use `name`."""
        return self.name

    def get_absolute_url(self):
        return reverse(
            'enrico:trip_detail',
            kwargs={
                'year': self.schedule.year,
                'slug': self.slug,
            },
        )

    @cached_property
    @admin.display(description=_('track'))
    def track_data(self):
        """Track data."""
        if self.track_file:
            return self.track_file.track_data.merged
        return None

    @cached_property
    @admin.display(description=_('start'))
    def start(self) -> Optional[Point]:
        """Track starting point."""
        if self.track_file:
            return Point(self.track_data[0])
        return None

    @cached_property
    @admin.display(description=_('arrival'))
    def end(self) -> Optional[Point]:
        """Track arrival point."""
        if self.track_file:
            return Point(self.track_data[-1])
        return None

    def get_meta_title(self) -> str:
        return str(self)

    def get_meta_description(self) -> str:
        """Build a description using stats."""
        description = _('Trip of {date}: {name}.').format(date=formats.localize(self.scheduled_date), name=self.name)
        if self.difficulty:
            difficulty = self.difficulty
            description += ' '
            description += _('Difficulty: {label} ({description}).').format(
                label=difficulty.label, description=difficulty.name
            )
        return description

    def get_meta_image(self):
        """Use self.flyer_picture when set, use current page as fallback."""
        return self.flyer_picture or super().get_meta_image()

    def get_meta_cropped_image(self) -> Optional[str]:
        """Override the crop settings if there's a flyer."""
        if self.flyer_picture:
            thumbnailer = get_thumbnailer(self.flyer_picture)
            return thumbnailer['trip-flyer'].url
        return super().get_meta_cropped_image()

    @admin.display(boolean=True, description=pgettext_lazy('trip', 'confirmed'))
    def is_confirmed(self) -> bool:
        return not self.canceled

    @admin.display(description=pgettext_lazy('trip', 'end date'))
    def end_date(self) -> date:
        return self.scheduled_date + timedelta(days=self.duration - 1)


class EscortAttendance(models.Model):
    """
    An escort on a trip.
    """

    escort = models.ForeignKey(
        Escort,
        on_delete=models.CASCADE,
        verbose_name=_('escort'),
        related_name='attendances',
        related_query_name='attendance',
    )

    trip = models.ForeignKey(
        Trip,
        on_delete=models.CASCADE,
        verbose_name=_('trip'),
        related_name='escort_attendances',
        related_query_name='escort_attendance',
    )

    role = models.ForeignKey(
        EscortRole,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name=_('role'),
        related_name='escort_attendances',
        related_query_name='escort_attendance',
    )

    class Meta:
        ordering = ('trip',)
        verbose_name = _('trip attendance')
        verbose_name_plural = _('trip attendance')
        constraints = [UniqueConstraint(name='unique_trip_escort', fields=['escort', 'trip'])]

    def __str__(self):
        """Escort and trip."""
        return _('{escort} on {trip}').format(
            escort=self.escort,
            trip=self.trip,
        )
