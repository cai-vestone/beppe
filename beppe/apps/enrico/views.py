"""
Enrico views
"""

from typing import Optional

from django.http import Http404
from django.utils.translation import gettext_lazy as _
from django.views import generic as views

from . import models


class TripScheduleListView(views.ListView):
    """
    Trip schedule list view.
    """

    model = models.TripSchedule
    context_object_name = 'schedule_list'
    template_name = 'enrico/tripschedule_list.html'


class TripScheduleDetailView(views.DetailView):
    """
    Trip schedule detail view.
    """

    model = models.TripSchedule
    context_object_name = 'schedule'
    template_name = 'enrico/tripschedule_detail.html'

    def get_object(self, queryset=None) -> Optional[models.TripSchedule]:
        """Get the trip schedule for a year."""
        if queryset is None:
            queryset = self.get_queryset()
        year = self.kwargs.get('year')
        try:
            obj = queryset.get(year=year)
        except queryset.model.DoesNotExist as e:
            raise Http404(
                _('No %(verbose_name)s found matching the query') % {'verbose_name': queryset.model._meta.verbose_name}
            ) from e
        return obj

    def get_context_data(self, **kwargs):
        """Add sorted trips."""
        schedule = self.get_object()
        if schedule is not None:
            kwargs['trips'] = schedule.trips.order_by('kind', 'scheduled_date')
        return super().get_context_data(**kwargs)


class TripDetailView(views.DetailView):
    """
    Trip detail view.
    """

    model = models.Trip
    context_object_name = 'trip'
    template_name = 'enrico/trip_detail.html'

    def get_queryset(self):
        year = self.kwargs['year']
        return super().get_queryset().filter(schedule__year=year)

    def get_context_data(self, **kwargs):
        kwargs['meta'] = self.get_object().as_meta(self.request)
        return super().get_context_data(**kwargs)
