"""
Enrico root urlconf
"""

from django.urls import path

from . import views

urlpatterns = [
    path(
        '',
        views.TripScheduleListView.as_view(),
        name='tripschedule_list',
    ),
    path(
        '<int:year>/',
        views.TripScheduleDetailView.as_view(),
        name='tripschedule_detail',
    ),
    path(
        '<int:year>/<slug:slug>/',
        views.TripDetailView.as_view(),
        name='trip_detail',
    ),
]
