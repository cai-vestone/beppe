"""
Escort admin interface
"""

from django.contrib import admin

from beppe.apps.enrico import models

__all__ = ['EscortAdmin']


@admin.register(models.Escort)
class EscortAdmin(admin.ModelAdmin):
    fields = (
        'is_group',
        'first_name',
        'last_name',
        'telephone',
        'email',
    )
    list_display = (
        'full_name',
        'telephone',
        'email',
    )
    search_fields = (
        'first_name',
        'last_name',
    )


@admin.register(models.EscortRole)
class EscortRoleAdmin(admin.ModelAdmin):
    list_display = ('name',)
    fields = ('name',)
