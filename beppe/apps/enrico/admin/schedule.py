"""
Trip admin interface
"""

from django.contrib import admin

from beppe.apps.enrico import models

__all__ = ['TripScheduleAdmin']


class TripInline(admin.TabularInline):
    """
    A trip in a schedule.
    """

    model = models.Trip
    fields = (
        'name',
        'scheduled_date',
        'difficulty',
        'kind',
        'canceled',
    )
    ordering = ('kind', 'scheduled_date')
    can_delete = False
    extra = 0
    show_change_link = True

    # def has_add_permission(self, request, obj=None):
    #     return False


@admin.register(models.TripSchedule)
class TripScheduleAdmin(admin.ModelAdmin):
    list_display = ('year', 'name')
    search_fields = ('year', 'name')
    inlines = (TripInline,)

    def get_inlines(self, request, obj=None):
        """No trip inlines for new schedules."""
        if obj is not None and obj.trips.exists():
            return super().get_inlines(request, obj)
        return []
