"""
Trip admin interface
"""

from adminsortable2.admin import SortableAdminMixin
from cms.admin.placeholderadmin import PlaceholderAdminMixin

from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from beppe.apps.enrico import models

__all__ = [
    'TripAdmin',
    'TripKindAdmin',
]


@admin.register(models.TripKind)
class TripKindAdmin(SortableAdminMixin, admin.ModelAdmin):
    fields = ('name',)
    list_display = ('order', 'name')
    list_display_links = ('name',)
    search_fields = ('name',)


class EscortInline(admin.TabularInline):
    model = models.EscortAttendance
    fields = (
        'escort',
        'role',
    )
    extra = 0
    autocomplete_fields = ('escort',)
    verbose_name = _('participant')
    verbose_name_plural = _('participants')


@admin.register(models.Trip)
class TripAdmin(PlaceholderAdminMixin, admin.ModelAdmin):
    fields = (
        'canceled',
        ('scheduled_date', 'duration'),
        ('schedule', 'kind'),
        'name',
        'slug',
        'difficulty',
        ('flyer', 'flyer_picture'),
        'track_file',
    )
    list_display = (
        'name',
        'scheduled_date',
        'difficulty',
        'schedule',
        'kind',
        'is_confirmed',
    )
    list_filter = (
        'schedule',
        'kind',
        'duration',
    )
    search_fields = ('name',)
    prepopulated_fields = {'slug': ('name',)}
    autocomplete_fields = ('difficulty',)
    inlines = (EscortInline,)
