"""
Enrico app config
"""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class EnricoConfig(AppConfig):
    name = 'beppe.apps.enrico'
    verbose_name = _('Trips')
