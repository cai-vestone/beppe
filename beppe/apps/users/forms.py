"""
Users forms
"""

from allauth.account import forms as allauth_forms

from django import forms
from django.contrib.auth import forms as admin_forms
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

User = get_user_model()


class UserChangeForm(admin_forms.UserChangeForm):
    """
    User change form
    """

    class Meta(admin_forms.UserChangeForm.Meta):
        model = User


class UserCreationForm(admin_forms.UserCreationForm):
    """
    User creation form.
    """

    error_message = admin_forms.UserCreationForm.error_messages.update(
        {'duplicate_username': _('This username has already been taken.')}
    )

    class Meta(admin_forms.UserCreationForm.Meta):
        model = User

    def clean_username(self) -> str:
        username = self.cleaned_data['username']

        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username

        raise ValidationError(self.error_messages['duplicate_username'])


class SignupForm(allauth_forms.SignupForm):
    """
    Signup form.
    """

    first_name = forms.CharField(
        label=_('First name'),
        required=True,
    )

    last_name = forms.CharField(
        label=_('Last name'),
        required=True,
    )

    def save(self, request):
        """Handle first and last name."""
        user = super().save(request)
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.save()
        return user
