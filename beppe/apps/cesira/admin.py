"""
Cesira admin
"""

from djangocms_blog.admin import PostAdmin
from djangocms_blog.models import Post

from django.contrib import admin
from django.utils.translation import gettext_lazy as _

admin.site.unregister(Post)


@admin.register(Post)
class CesiraPostAdmin(PostAdmin):
    """
    Custom post admin.
    """

    _fieldsets = [
        (
            None,
            {
                'fields': [
                    'publish',
                    'title',
                    'slug',
                    'categories',
                    'tags',
                ]
            },
        ),
        (None, {'fields': [[]]}),  # left empty for sites, author and related fields
        (
            _('Image'),
            {
                'fields': ['main_image', 'main_image_thumbnail', 'main_image_full'],
                'classes': ('collapse',),
            },
        ),
        (
            _('Dates'),
            {
                'fields': [
                    'date_published',
                    'date_published_end',
                    'date_featured',
                ],
                'classes': ('collapse',),
            },
        ),
        (
            _('SEO'),
            {
                'fields': ['meta_description', 'meta_title'],
                'classes': ('collapse',),
            },
        ),
        (
            _('Advanced settings'),
            {
                'fields': ['app_config', 'enable_comments'],
                'classes': ('collapse',),
            },
        ),
    ]

    _fieldset_extra_fields_position = {
        'abstract': (0, 1),
        'post_text': (0, 1),
        'sites': (1, 1, 0),
        'author': None,
        'enable_liveblog': (2, 1, 2),
        'related': (1, 1, 0),
    }
