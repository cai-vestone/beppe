"""
Cesira app config
"""

from django.apps import AppConfig


class CesiraConfig(AppConfig):
    name = 'beppe.apps.cesira'
    verbose_name = 'Cesira'
