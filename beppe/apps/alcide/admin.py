"""
Alcide admin
"""

from django.contrib import admin

from . import models


@admin.register(models.MagazineIssue)
class MagazineIssueAdmin(admin.ModelAdmin):
    """
    Magazine issue admin.
    """

    fields = (
        'number',
        'year',
        'name',
        'calameo_url',
        'cover_picture',
    )
    list_display = ('number', 'year')
