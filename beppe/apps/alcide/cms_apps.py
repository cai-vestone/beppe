"""
Alcide django-cms apphooks
"""

from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

from django.utils.translation import gettext_lazy as _

from .urls import urlpatterns


@apphook_pool.register
class AlcideApphook(CMSApp):
    app_name = 'alcide'
    name = _('Magazine')

    def get_urls(self, page=None, language=None, **kwargs):
        return urlpatterns
