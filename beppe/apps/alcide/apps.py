"""
Alcide app config
"""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class AlcideConfig(AppConfig):
    name = 'beppe.apps.alcide'
    verbose_name = _('Magazine')
