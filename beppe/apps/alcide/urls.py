"""
Alcide root urlconf
"""

from django.urls import path

from . import views

urlpatterns = [
    path(
        '',
        views.MagazineIssueListView.as_view(),
        name='magazineissue_list',
    ),
]
