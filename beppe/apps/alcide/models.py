"""
Alcide models
"""

from filer.fields.image import FilerImageField

from django.db import models
from django.db.models import UniqueConstraint
from django.utils.translation import gettext_lazy as _


class MagazineIssue(models.Model):
    """
    Magazine issue.
    """

    number = models.PositiveIntegerField(_('number'))

    year = models.PositiveSmallIntegerField(_('year'))

    name = models.CharField(_('name'), max_length=200, blank=True)

    calameo_url = models.URLField(_('address on Calaméo'), unique=True)

    cover_picture = FilerImageField(
        on_delete=models.CASCADE,
        verbose_name=_('cover picture'),
        related_name='+',
    )

    class Meta:
        verbose_name = _('magazine issue')
        verbose_name_plural = _('magazine issues')
        ordering = ('year', 'number')
        constraints = [
            UniqueConstraint(fields=['number', 'year'], name='unique_number_year'),
        ]

    def __str__(self):
        return self.name or _('Number {number} ({year})').format(
            number=self.number,
            year=self.year,
        )

    def get_absolute_url(self):
        return self.calameo_url
