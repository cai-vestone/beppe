"""
Alcide views
"""

from django.views import generic as views

from .models import MagazineIssue


class MagazineIssueListView(views.ListView):
    """
    Trail list.
    """

    model = MagazineIssue
    context_object_name = 'issue_list'
    template_name = 'alcide/magazineissue_list.html'
