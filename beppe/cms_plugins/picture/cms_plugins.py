"""
Picture django-cms plugins
"""

from cms.plugin_pool import plugin_pool
from djangocms_picture import cms_plugins
from easy_thumbnails.files import get_thumbnailer

from django.utils.translation import gettext_lazy as _

from .forms import PicturePluginForm
from .models import PicturePluginModel

plugin_pool.unregister_plugin(cms_plugins.PicturePlugin)


@plugin_pool.register_plugin
class PicturePlugin(cms_plugins.PicturePlugin):
    """
    Picture plugin.
    """

    name = _('Picture')
    model = PicturePluginModel
    form = PicturePluginForm
    render_template = 'beppe/cms_plugins/picture/picture.html'
    fieldsets = (
        (
            None,
            {
                'fields': (
                    'picture',
                    'external_picture',
                    'alt_text',
                    'caption_text',
                    'alignment',
                )
            },
        ),
        (
            _('Cropping settings'),
            {
                'classes': ('collapse',),
                'fields': (
                    'use_automatic_scaling',
                    'thumbnail_options',
                    'use_no_cropping',
                    'use_crop',
                    'use_upscale',
                ),
            },
        ),
        (
            _('Advanced settings'),
            {
                'classes': ('collapse',),
                'fields': (
                    'template',
                    'use_responsive_image',
                    ('width', 'height'),
                    'attributes',
                ),
            },
        ),
    )

    def get_render_template(self, context, instance, placeholder):
        return self.render_template

    def render(self, context, instance, placeholder):
        """Use the right alignment classes."""
        context = super().render(context, instance, placeholder)

        classes = 'bp-c-picture'
        if instance.alignment:
            classes += f' bp-m-align-{instance.alignment} '
            # Set the class attribute to include the alignment html class
            # This is done to leverage the attributes_str property
        instance.attributes['class'] = classes

        if instance.picture:
            thumbnail_options = context['picture_size']
            thumbnailer = get_thumbnailer(instance.picture)
            thumbnail = thumbnailer.get_thumbnail(thumbnail_options)
            context['thumbnail'] = thumbnail

        return context
