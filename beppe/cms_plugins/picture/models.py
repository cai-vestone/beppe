"""
Picture django-cms plugin models
"""

from djangocms_attributes_field.fields import AttributesField
from djangocms_picture.models import AbstractPicture

from django.db import models
from django.utils.translation import gettext_lazy as _


class PicturePluginModel(AbstractPicture):
    """
    Picture plugin model.
    """

    attributes = AttributesField(
        verbose_name=_('attributes'),
        blank=True,
        excluded_keys=['src', 'alt', 'class', 'width', 'height'],
    )

    alt_text = models.CharField(
        _('alt text'),
        max_length=255,
        blank=True,
    )

    def get_alt_text(self):
        """Get a usable alternative text."""
        if self.alt_text:
            return self.alt_text
        if self.picture:
            return self.picture.default_alt_text or ''  # could be None
        return ''

    def get_caption(self):
        """Get a usable caption text."""
        if self.caption_text:
            return self.caption_text
        if self.picture:
            return self.picture.default_caption or ''  # could be None
        return ''

    class Meta:
        abstract = False
        verbose_name = _('picture')
        verbose_name_plural = _('pictures')
