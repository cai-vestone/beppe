"""
Picture django-cms plugin forms
"""

from djangocms_picture.forms import PictureForm

from django.utils.translation import gettext_lazy as _


class PicturePluginForm(PictureForm):
    """
    Picture plugin form.
    """

    def __init__(self, *args, **kwargs):
        """Override some labels."""
        super().__init__(*args, **kwargs)

        if 'caption_text' in self.fields:
            self.fields['caption_text'].help_text = None

        if 'external_picture' in self.fields:
            self.fields['external_picture'].help_text = _('Use an image from another site')

        if 'alignment' in self.fields:
            self.fields['alignment'].help_text = None

        if 'use_automatic_scaling' in self.fields:
            self.fields['use_automatic_scaling'].label = _('Automatic scaling')
            self.fields['use_automatic_scaling'].help_text = _(
                'Uses the placeholder dimensions to automatically calculate the size.'
            )

        if 'thumbnail_options' in self.fields:
            self.fields['thumbnail_options'].label = _('Thumbnail options')
            self.fields['thumbnail_options'].help_text = None

        if 'use_no_cropping' in self.fields:
            self.fields['use_no_cropping'].help_text = _('Outputs the raw image without cropping.')

        if 'use_crop' in self.fields:
            self.fields['use_crop'].label = _('Crop image')
            self.fields['use_crop'].help_text = None

        if 'use_upscale' in self.fields:
            self.fields['use_upscale'].label = _('Upscale image')
            self.fields['use_upscale'].help_text = None
