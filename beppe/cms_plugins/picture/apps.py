"""
Picture app config
"""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class PictureConfig(AppConfig):
    name = 'beppe.cms_plugins.picture'
    verbose_name = _('Picture')
