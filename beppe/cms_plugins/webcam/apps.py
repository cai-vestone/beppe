"""
webcam app config
"""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class PictureConfig(AppConfig):
    name = 'beppe.cms_plugins.webcam'
    verbose_name = _('Webcam')
