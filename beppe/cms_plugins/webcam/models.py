"""
webcam models
"""

import glob
import os
from dataclasses import dataclass
from typing import Optional

from cms.models import CMSPlugin
from djangocms_attributes_field.fields import AttributesField
from djangocms_picture.models import get_alignment
from filer.fields.image import FilerImageField

from django.conf import settings
from django.contrib import admin
from django.db import models
from django.utils.functional import cached_property
from django.utils.timezone import datetime
from django.utils.translation import gettext_lazy as _


@dataclass(frozen=True)
class Snapshot:
    """Webcam snapshot."""

    path: str

    @property
    def created(self) -> datetime:
        return datetime.fromtimestamp(os.path.getmtime(self.path))

    @cached_property
    def absolute_url(self) -> str:
        """Absolute URL."""
        return self.path.replace(f'{settings.MEDIA_ROOT}/', settings.MEDIA_URL)


class SnapshotPluginModel(CMSPlugin):
    """
    Webcam snapshot plugin model.
    """

    caption_text = models.CharField(_('caption'), blank=True)

    default_picture = FilerImageField(
        verbose_name=_('default image'),
        on_delete=models.CASCADE,
        related_name='+',
        help_text=_('Used when no webcam picture is available.')
    )

    alignment = models.CharField(
        verbose_name=_('alignment'),
        choices=get_alignment(),
        blank=True,
        max_length=255,
    )

    attributes = AttributesField(
        verbose_name=_('attributes'),
        blank=True,
        excluded_keys=['src'],
    )

    class Meta:
        verbose_name = _('webcam snapshot plugin')
        verbose_name_plural = _('webcam snapshot plugins')

    def __str__(self) -> str:
        """Title."""
        return self.caption_text

    @property
    @admin.display(description=_('snapshot URL'))
    def snapshot_url(self) -> Optional[Snapshot]:
        """Last webcam snapshot URL."""
        root_path = f'{settings.MEDIA_ROOT}/webcam'
        paths = list(
            filter(os.path.isfile, glob.glob(f'{root_path}/**/*.jpg', recursive=True))
        )
        if paths:
            paths.sort(key=lambda path: os.path.getmtime(path))
            return Snapshot(paths[-1])
        return None

    @property
    def picture(self):
        return self.default_picture

    @property
    def img_src(self) -> str:
        snapshot = self.snapshot_url
        if snapshot:
            return snapshot.absolute_url
        return self.default_picture.url

    def get_caption(self) -> Optional[str]:
        return self.caption_text or None
