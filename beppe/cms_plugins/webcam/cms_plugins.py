"""
webcam CMS plugins
"""

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from django.utils.translation import gettext_lazy as _

from beppe.cms_plugins.webcam import models


@plugin_pool.register_plugin
class SnapshotPlugin(CMSPluginBase):
    """
    Webcam snapshot plugin.
    """

    model = models.SnapshotPluginModel
    allow_children = False
    render_template = 'beppe/cms_plugins/picture/picture.html'
    name = _('Webcam')
    fieldsets = (
        (
            None, {
                'fields': ('default_picture', 'caption_text', 'alignment',)
            }
        ),
        (
            _('Advanced settings'), {
                'classes': ('collapse',),
                'fields': ('attributes',)
            },
        ),
    )

    def render(self, context, instance, placeholder):
        """Use the right alignment classes."""
        classes = 'bp-c-picture'
        if instance.alignment:
            classes += f' bp-m-align-{instance.alignment} '
            # Set the class attribute to include the alignment html class
            # This is done to leverage the attributes_str property
        instance.attributes['class'] = classes

        context.update({
            'instance': instance,
            'thumbnail': {
                'url': instance.img_src,
            },
        })

        return super().render(context, instance, placeholder)
