# Generated by Django 3.2.10 on 2022-01-03 17:00

from django.db import migrations, models
import django.db.models.deletion
import filer.fields.folder


class Migration(migrations.Migration):

    dependencies = [
        ('filer', '0014_folder_permission_choices'),
        ('picture_gallery', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='polaroidspluginmodel',
            name='folder',
            field=filer.fields.folder.FilerFolderField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='filer.folder', verbose_name='folder'),
        ),
        migrations.AddField(
            model_name='polaroidspluginmodel',
            name='picture_count',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='picture count'),
        ),
    ]
