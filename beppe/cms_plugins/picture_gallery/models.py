"""
Picture gallery django-cms plugin models
"""

from cms.models import CMSPlugin
from filer.fields.folder import FilerFolderField

from django.db import models
from django.utils.translation import gettext_lazy as _


class PictureGalleryPluginModel(CMSPlugin):
    """
    Base picture gallery plugin model.
    """

    # FIXME: `folder` and `picture_count` belong in a form

    folder = FilerFolderField(
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='+',
        verbose_name=_('folder'),
    )

    picture_count = models.PositiveSmallIntegerField(_('picture count'), null=True, blank=True)

    class Meta:
        abstract = True

    def __str__(self):
        """Use the number of children."""
        return str(self.child_count)

    @property
    def child_count(self):
        """Child count."""
        return len(self.child_plugin_instances or [])


class PolaroidsPluginModel(PictureGalleryPluginModel):
    """
    Polaroid container plugin model.
    """

    class Meta:
        verbose_name = _('polaroids')
        verbose_name_plural = _('polaroids')
