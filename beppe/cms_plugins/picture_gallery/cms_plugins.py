"""
Picture gallery django-cms plugins
"""

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from filer.models.abstract import BaseImage

from django.utils.translation import gettext_lazy as _

from beppe.cms_plugins.picture.cms_plugins import PicturePlugin
from beppe.cms_plugins.picture.models import PicturePluginModel

from . import models


class PictureGalleryPlugin(CMSPluginBase):
    """
    Base picture gallery plugin class.
    """

    allow_children = True
    parent_classes = None
    child_classes = ['PicturePlugin']
    module = _('Picture gallery')
    fieldsets = (
        (
            _('Picture import'),
            {
                'fields': ('folder', 'picture_count'),
            },
        ),
    )

    def save_model(self, request, obj, form, change):
        """Import pictures."""
        folder = obj.folder
        picture_count = obj.picture_count
        obj.folder = None
        obj.picture_count = None
        response = super().save_model(request, obj, form, change)
        old_pictures = [
            instance.picture for instance, _ in [plugin.get_plugin_instance() for plugin in obj.get_children()]
        ]
        pictures = (
            folder.all_files.order_by('original_filename')
            .instance_of(BaseImage)
            .exclude(pk__in=[p.pk for p in old_pictures])
            if folder is not None
            else []
        )
        for picture in pictures[slice(0, picture_count)]:
            picture_plugin = PicturePluginModel(
                parent=obj,
                placeholder=obj.placeholder,
                language=obj.language,
                # position=CMSPlugin.objects.filter(parent=obj).count(),
                plugin_type=PicturePlugin.__name__,
                picture=picture,
            )
            picture_plugin.save()

        return response


@plugin_pool.register_plugin
class PolaroidsPlugin(PictureGalleryPlugin):
    """
    Polaroid container plugin.
    """

    name = _('Polaroids')
    model = models.PolaroidsPluginModel
    render_template = 'cms_plugins/picture_gallery/polaroids.html'
