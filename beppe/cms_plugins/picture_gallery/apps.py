"""
Picture gallery app config
"""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class PictureGalleryConfig(AppConfig):
    name = 'beppe.cms_plugins.picture_gallery'
    verbose_name = _('Picture gallery')
