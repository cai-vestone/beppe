"""
Callout models
"""

from cms.models import CMSPlugin

from django.db import models
from django.utils.translation import gettext_lazy as _


class CalloutPluginModel(CMSPlugin):
    """
    Callout plugin model.
    """

    SECONDARY = 'secondary'
    PRIMARY = 'primary'
    SUCCESS = 'success'
    WARNING = 'warning'
    ALERT = 'alert'
    COLOR_CHOICES = (
        (SECONDARY, _('Secondary')),
        (PRIMARY, _('Primary')),
        (SUCCESS, _('Success')),
        (WARNING, _('Warning')),
        (ALERT, _('Alert')),
    )

    SMALL = 'small'
    LARGE = 'large'
    SIZING_CHOICES = (
        (SMALL, _('Small')),
        (LARGE, _('Large')),
    )

    color = models.CharField(_('color'), max_length=20, blank=True, choices=COLOR_CHOICES)

    sizing = models.CharField(_('size'), max_length=20, blank=True, choices=SIZING_CHOICES)

    class Meta:
        abstract = False
        verbose_name = _('callout')
        verbose_name_plural = _('callout')

    def __str__(self):
        if self.color:
            return self.get_color_display()
        return ''
