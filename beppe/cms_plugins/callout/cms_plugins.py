"""
Callout django-cms plugins
"""

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from django.utils.translation import gettext_lazy as _

from .models import CalloutPluginModel


@plugin_pool.register_plugin
class CalloutPlugin(CMSPluginBase):
    """
    Callout plugin.
    """

    name = _('Callout')
    model = CalloutPluginModel
    allow_children = True
    child_classes = ['TextPlugin']
    render_template = 'cms_plugins/callout.html'
    fields = (
        'color',
        'sizing',
    )
