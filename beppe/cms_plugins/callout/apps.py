"""
Callout app config
"""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class LinkConfig(AppConfig):
    name = 'beppe.cms_plugins.callout'
    verbose_name = _('Callout')
