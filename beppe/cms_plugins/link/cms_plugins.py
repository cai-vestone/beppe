"""
Link django-cms plugins
"""

from cms.plugin_pool import plugin_pool
from djangocms_link import cms_plugins

from django.utils.translation import gettext_lazy as _

from .forms import LinkPluginForm
from .models import LinkPluginModel

plugin_pool.unregister_plugin(cms_plugins.LinkPlugin)


@plugin_pool.register_plugin
class LinkPlugin(cms_plugins.LinkPlugin):
    """
    Link plugin.
    """

    name = _('Link')
    model = LinkPluginModel
    form = LinkPluginForm
    fieldsets = [
        (None, {'fields': (('name', 'target'),)}),
        (
            _('Destination'),
            {
                'fields': (
                    'external_link',
                    'internal_link',
                    'trail',
                    'mailto',
                    'phone',
                    # 'anchor',
                    # 'file_link',
                )
            },
        ),
        (
            _('Advanced settings'),
            {
                'classes': ('collapse',),
                'fields': (
                    'template',
                    'attributes',
                ),
            },
        ),
    ]

    def get_render_template(self, context, instance, placeholder):
        return f'cms_plugins/link/{instance.template}/link.html'
