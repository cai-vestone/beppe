"""
Link models
"""

from djangocms_link.models import AbstractLink

from django.core.exceptions import ValidationError
from django.db import models
from django.utils.encoding import force_str
from django.utils.translation import gettext_lazy as _

from beppe.apps.lodovico.models import Trail


class LinkPluginModel(AbstractLink):
    """
    Link django-cms plugin model.
    """

    trail = models.ForeignKey(
        Trail,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name=_('trail'),
        related_name='links',
        related_query_name='link',
    )

    def get_link(self):
        """Handle links to trails."""
        link = super().get_link()
        if not link and self.trail:
            link = self.trail.get_absolute_url()
        return link

    def clean(self):
        """Ignore `file_link`."""
        super(AbstractLink, self).clean()  # bypass AbstractLink.clean
        field_names = (
            'external_link',
            'internal_link',
            'trail',
            'mailto',
            'phone',
        )

        link_fields = {key: getattr(self, key) for key in field_names}
        link_field_verbose_names = {
            key: force_str(self._meta.get_field(key).verbose_name) for key in link_fields.keys()
        }
        provided_link_fields = {key: value for key, value in link_fields.items() if value}

        if len(provided_link_fields) > 1:
            # Too many fields have a value.
            verbose_names = sorted(link_field_verbose_names.values())
            error_msg = _('Only one of {0} or {1} may be given.').format(
                ', '.join(verbose_names[:-1]),
                verbose_names[-1],
            )
            errors = {}.fromkeys(provided_link_fields.keys(), error_msg)
            raise ValidationError(errors)

        if (
            len(provided_link_fields) == 0
            # and not self.anchor
            and not self.link_is_optional
        ):

            raise ValidationError(_('Please provide a link destination.'))

    class Meta:
        abstract = False
        verbose_name = _('link')
        verbose_name_plural = _('links')
