"""
Link app config
"""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class LinkConfig(AppConfig):
    name = 'beppe.cms_plugins.link'
    verbose_name = _('Link')
