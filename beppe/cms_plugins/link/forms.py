"""
Link plugin forms
"""

from djangocms_link.forms import LinkForm

from django.utils.translation import gettext_lazy as _


class LinkPluginForm(LinkForm):
    """
    Link plugin form.
    """

    def __init__(self, *args, **kwargs):
        """Override some labels."""
        super().__init__(*args, **kwargs)
        self.fields['name'].label = _('label')

        self.fields['external_link'].label = _('external link')
        self.fields['external_link'].help_text = _('Link to a page on another site')

        self.fields['internal_link'].label = _('internal link')

        self.fields['phone'].label = _('phone number')
