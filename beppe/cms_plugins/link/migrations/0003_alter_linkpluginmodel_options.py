# Generated by Django 3.2.13 on 2022-05-07 11:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('link', '0002_trail_link'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='linkpluginmodel',
            options={'verbose_name': 'link', 'verbose_name_plural': 'links'},
        ),
    ]
