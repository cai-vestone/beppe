"""
Duration utilities
"""

from typing import Tuple


def _get_duration_components(duration) -> Tuple[int, int]:
    seconds = int(duration.total_seconds())

    minutes = seconds // 60
    # seconds = seconds % 60

    hours = minutes // 60
    minutes = minutes % 60

    return hours, minutes


def trail_duration_string(duration) -> str:
    """Version of `str(timedelta)` for trail durations."""
    hours, minutes = _get_duration_components(duration)
    return f'{hours:02d}:{minutes:02d}'
