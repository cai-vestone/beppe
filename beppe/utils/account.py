"""
Beppe account utilities
"""

from beppe.apps.users.models import User


def user_display(user: User) -> str:
    return user.get_full_name() or user.username
