"""
Page metadata utilities
"""

from typing import Optional

from cms.utils import get_language_from_request
from djangocms_page_meta.models import PageMeta, TitleMeta
from filer.models.abstract import BaseImage


def get_page_meta_image(request, page) -> Optional[BaseImage]:
    language = get_language_from_request(request)
    title = page.get_title_obj(language)
    image = None
    try:
        titlemeta = title.titlemeta
        if titlemeta.image:
            image = titlemeta.image
    except (TitleMeta.DoesNotExist, AttributeError):
        pass
    if image is None:
        try:
            pagemeta = page.pagemeta
            if pagemeta.image:
                image = pagemeta.image
        except PageMeta.DoesNotExist:
            pass
    return image


def get_page_meta_description(request, page) -> Optional[BaseImage]:
    language = get_language_from_request(request)
    title = page.get_title_obj(language)
    description = title.meta_description.strip()
    try:
        titlemeta = title.titlemeta
        if titlemeta.description:
            description = titlemeta.description.strip()
    except (TitleMeta.DoesNotExist, AttributeError):
        pass
    return description
