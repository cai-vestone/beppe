"""
Functions to parse datetime objects
"""

import datetime
from typing import Optional

from django.utils.dateparse import parse_duration
from django.utils.regex_helper import _lazy_re_compile

# FIXME: mypy: Module "django.utils.regex_helper" has no attribute "_lazy_re_compile"

__all__ = [
    'parse_duration',
    'parse_trail_duration',
]

trail_duration_re = _lazy_re_compile(r'^' r'(?P<hours>\d+)' r'(?:\:(?P<minutes>\d+))?' r'$')


def parse_trail_duration(value) -> Optional[datetime.timedelta]:
    """
    Parse a duration string and return a datetime.timedelta.

    The preferred format for durations in Beppe is '%H:%M'.
    The preferred format for durations in Django is '%d %H:%M:%S.%f'.

    Also supports ISO 8601 representation and PostgreSQL's day-time interval
    format.
    """
    match = trail_duration_re.match(value)
    if match:
        kw = match.groupdict()
        kw = {k: float(v.replace(',', '.')) for k, v in kw.items() if v is not None}
        return datetime.timedelta(**kw)
    return parse_duration(value)
