"""
Beppe template context processors
"""

from cms.exceptions import NoHomeFound
from cms.models import Page

from django.conf import settings

try:
    HOMEPAGE_URL = Page._default_manager.get_home().get_absolute_url()
except NoHomeFound:
    HOMEPAGE_URL = '/'


def settings_context(_request):
    """Settings available by default to the templates context."""
    return {
        'DEBUG': settings.DEBUG,
        'HEADER_BACKGROUNDS': settings.BEPPE_HEADER_BACKGROUNDS,
        'LOGO_CAPTION': settings.BEPPE_LOGO_CAPTION,
        'HOMEPAGE_URL': HOMEPAGE_URL,
    }
