"""
Beppe meta-related models
"""

from dataclasses import dataclass
from typing import Iterable, Optional

from cms.utils import get_current_site
from cms.utils.i18n import get_language_list, get_language_object, get_public_languages
from easy_thumbnails.files import get_thumbnailer
from filer.models.abstract import BaseImage
from meta.models import ModelMeta

from django.conf import settings
from django.utils.text import Truncator
from django.utils.translation import activate, get_language_from_request

from beppe.utils.meta import get_page_meta_description, get_page_meta_image


@dataclass(frozen=True)
class Alternate:
    lang: str
    href: str


class BeppeModelMeta(ModelMeta):
    """
    A specialized ModelMeta subclass.
    """

    _metadata = {
        'title': 'get_meta_title',
        'description': 'get_meta_truncated_description',
        'image': 'get_meta_cropped_image',
    }

    def get_meta_title(self) -> str:
        raise NotImplementedError('{klass}.get_meta_title'.format(klass=self.__class__))

    def get_meta_description(self) -> Optional[str]:
        request = self.get_request()
        return get_page_meta_description(request, request.current_page)

    def get_meta_image(self) -> Optional[BaseImage]:
        request = self.get_request()
        return get_page_meta_image(request, request.current_page)

    def get_meta_cropped_image(self) -> Optional[str]:
        image = self.get_meta_image()
        if image:
            thumbnailer = get_thumbnailer(image)
            return thumbnailer['meta_image'].url
        return None

    def get_meta_truncated_description(self) -> Optional[str]:
        """Truncate description."""
        description = self.get_meta_description()
        if description:
            return Truncator(description).chars(settings.PAGE_META_DESCRIPTION_LENGTH)
        return None

    def get_alternates(self, request) -> Iterable[Alternate]:
        site = get_current_site()
        original_language = get_language_from_request(request)

        if request.user.is_staff:
            languages = get_language_list(site_id=site.pk)
        else:
            languages = get_public_languages(site_id=site.pk)

        for language in languages:
            obj = get_language_object(language, site_id=site.pk)
            language_code = obj['code']
            if language_code != original_language:
                activate(language_code)
                yield Alternate(lang=language_code, href=self.get_absolute_url())

        activate(original_language)
