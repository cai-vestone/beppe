"""
Beppe admin appconfigs
"""

from django.contrib.admin.apps import AdminConfig


class BeppeAdminConfig(AdminConfig):
    """
    Custom ``AdminConfig`` for Beppe
    """

    default_site = 'beppe.admin.site.BeppeAdminSite'
