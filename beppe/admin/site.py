"""
Beppe admin sites
"""

from django.conf import settings
from django.contrib import admin


class BeppeAdminSite(admin.AdminSite):
    """
    Custom `AdminSite` for Beppe
    """

    site_header = settings.ADMIN_SITE_HEADER
    site_title = settings.ADMIN_SITE_TITLE
    index_title = settings.ADMIN_INDEX_TITLE
    enable_nav_sidebar = settings.ADMIN_SHOW_SIDEBAR
